
#include "stdint.h"
#include "stm32l4xx_hal.h"

 #define _MCP23017_INTERRUPT_SUPPORT_ ///< Enables support for MCP23017 interrupts.

#define INPUT 0
#define INPUT_PULLUP 1
int printf0 (const char *fmt, ...);
#define uprintf printf0
/**
 * Registers addresses.
 * The library use addresses for IOCON.BANK = 0.
 * See "3.2.1 Byte mode and Sequential mode".
 */
/* data */
#define REGISTER_IODIR_A	 0x00 		///< Controls the direction of the data I/O for port A.
#define REGISTER_IODIR_B	 0x01			///< Controls the direction of the data I/O for port B.
#define REGISTER_IPOL_A		 0x02			///< Configures the polarity on the corresponding GPIO_ port bits for port A.
#define REGISTER_IPOL_B		 0x03			///< Configures the polarity on the corresponding GPIO_ port bits for port B.
#define REGISTER_GPINTEN_A	 0x04			///< Controls the interrupt-on-change for each pin of port A.
#define REGISTER_GPINTEN_B	 0x05			///< Controls the interrupt-on-change for each pin of port B.
#define REGISTER_DEFVAL_A	 0x06			///< Controls the default comparaison value for interrupt-on-change for port A.
#define REGISTER_DEFVAL_B	 0x07			///< Controls the default comparaison value for interrupt-on-change for port B.
#define REGISTER_INTCON_A	 0x08			///< Controls how the associated pin value is compared for the interrupt-on-change for port A.
#define REGISTER_INTCON_B	 0x09			///< Controls how the associated pin value is compared for the interrupt-on-change for port B.
#define REGISTER_IOCON		 0x0A			///< Controls the device.
#define REGISTER_GPPU_A		 0x0C			///< Controls the pull-up resistors for the port A pins.
#define REGISTER_GPPU_B		 0x0D			///< Controls the pull-up resistors for the port B pins.
#define REGISTER_INTF_A		 0x0E			///< Reflects the interrupt condition on the port A pins.
#define REGISTER_INTF_B		 0x0F			///< Reflects the interrupt condition on the port B pins.
#define REGISTER_INTCAP_A	 0x10			///< Captures the port A value at the time the interrupt occured.
#define REGISTER_INTCAP_B	 0x11			///< Captures the port B value at the time the interrupt occured.
#define REGISTER_GPIO_A		 0x12			///< Reflects the value on the port A.
#define REGISTER_GPIO_B		 0x13			///< Reflects the value on the port B.
#define REGISTER_OLAT_A		 0x14			///< Provides access to the port A output latches.
#define REGISTER_OLAT_B		 0x15			///< Provides access to the port B output latches.

#define CHANGE 0
#define FALLING 1
#define RISING 2

#define PIN_RST 0
#define PIN_A0 1
#define PIN_RW 2
#define PIN_E  3
#define PIN_A4 4
#define PIN_A5 5
#define PIN_A6 6
#define PIN_A7 7
#define PIN_B7 7+8
#define PIN_B6 6+8
#define PIN_B5 5+8
#define PIN_B4 4+8
#define PIN_BLA 3+8

#define PIN_B2 2+8


void register_Write(uint8_t *port,uint8_t pin, uint8_t state);
void MCP23017_writePorts(uint8_t *value);
void MCP23017_readPorts(uint8_t *value);

	/**
	 * Initializes the chip with the default configuration.
	 * Enables Byte mode (IOCON.BANK = 0 and IOCON.SEQOP = 1).
	 * Enables pull-up resistors for all pins. This will only be effective for input pins.
	 *
	 * See "3.2.1 Byte mode and Sequential mode".
	 */
void MCP23017_init();
	/**
	 * Controls the pins direction on a whole port at once.
	 *
	 * 1 = Pin is configured as an input.
	 * 0 = Pin is configured as an output.
	 *
	 * See "3.5.1 I/O Direction register".
	 */
	void MCP23017_portMode(uint8_t port, uint8_t directions, uint8_t pullups, uint8_t inverted);
	/**
	 * Controls a single pin direction.
	 * Pin 0-7 for port A, 8-15 fo port B.
	 *
	 * 1 = Pin is configured as an input.
	 * 0 = Pin is configured as an output.
	 *
	 * See "3.5.1 I/O Direction register".
	 *
	 * Beware!
	 * On Arduino platform, INPUT = 0, OUTPUT = 1, which is the inverse
	 * of the MCP23017 definition where a pin is an input if its IODIR bit is set to 1.
	 * This library pinMode function behaves like Arduino's standard pinMode for consistency.
	 * [ OUTPUT | INPUT | INPUT_PULLUP ]
	 */
	void MCP23017_pinMode(uint8_t pin, uint8_t mode, uint8_t inverted);

	/**
	 * Writes a single pin state.
	 * Pin 0-7 for port A, 8-15 for port B.
	 *
	 * 1 = Logic-high
	 * 0 = Logic-low
	 *
	 * See "3.5.10 Port register".
	 */
	void MCP23017_digitalWrite(uint8_t pin, uint8_t state);
	/**
	 * Reads a single pin state.
	 * Pin 0-7 for port A, 8-15 for port B.
	 *
	 * 1 = Logic-high
	 * 0 = Logic-low
	 *
	 * See "3.5.10 Port register".
	 */
	uint8_t MCP23017_digitalRead(uint8_t pin);

	/**
	 * Writes pins state to a whole port.
	 *
	 * 1 = Logic-high
	 * 0 = Logic-low
	 *
	 * See "3.5.10 Port register".
	 */
	void MCP23017_writePort(uint8_t port, uint8_t value);

	/**
	 * Reads pins state for a whole port.
	 *
	 * 1 = Logic-high
	 * 0 = Logic-low
	 *
	 * See "3.5.10 Port register".
	 */
	uint8_t MCP23017_readPort(uint8_t port);
	/**
	 * Reads pins state for both ports.
	 *
	 * 1 = Logic-high
	 * 0 = Logic-low
	 *
	 * See "3.5.10 Port register".
	 */
	uint16_t MCP23017_read();

	/**
	 * Writes a single register value.
	 */
	void MCP23017_writeRegister(uint16_t reg, uint8_t value);
	/**
	 * Writes values to a register pair.
	 *
	 * For portA and portB variable to effectively match the desired port,
	 * you have to supply a portA register address to reg. Otherwise, values
	 * will be reversed due to the way the MCP23017 works in Byte mode.
	 */
	void MCP23017_writeRegisters(uint8_t reg, uint8_t portA, uint8_t portB);
	/**
	 * Reads a single register value.
	 */
	uint8_t MCP23017_readRegister(uint8_t reg);
	/**
	 * Reads the values from a register pair.
	 *
	 * For portA and portB variable to effectively match the desired port,
	 * you have to supply a portA register address to reg. Otherwise, values
	 * will be reversed due to the way the MCP23017 works in Byte mode.
	 */
	void MCP23017_readRegisters(uint8_t reg, uint8_t *portA, uint8_t *portB);

#ifdef _MCP23017_INTERRUPT_SUPPORT_

	/**
	 * Controls if the two interrupt pins mirror each other.
	 * See "3.6 Interrupt Logic".
	 */
	#define Separated 0			///< Interrupt pins are kept independent
	#define Or 0b01000000		///< Interrupt pins are mirrored

	/**
	 * Controls how the interrupt pins act with each other.
	 * If intMode is SEPARATED, interrupt conditions on a port will cause its respective INT pin to active.
	 * If intMode is OR, interrupt pins are OR'ed so an interrupt on one of the port will cause both pints to active.
	 *
	 * Controls the IOCON.MIRROR bit.
	 * See "3.5.6 Configuration register".
	 */
	void MCP23017_interruptMode(uint8_t intMode);
	/**
	 * Configures interrupt registers using an Arduino-like API.
	 * mode can be one of CHANGE, FALLING or RISING.
	 */
	void MCP23017_interrupt(uint8_t port, uint8_t mode);
	void MCP23017_interrupt_pin(uint8_t port, uint8_t pin, uint8_t mode);

	/**
	 * Disable interrupts for the specified port.
	 */
	void MCP23017_disableInterrupt(uint8_t port);
	/**
	 * Reads which pin caused the interrupt.
	 */
	void MCP23017_interruptedBy(uint8_t *portA, uint8_t *portB);
	/**
	 * Clears interrupts on both ports.
	 */
	void MCP23017_clearInterrupts_all();

	/**
	 * Clear interrupts on both ports. Returns port values at the time the interrupt occured.
	 */
	void MCP23017_clearInterrupts(uint8_t *portA, uint8_t *portB);


#endif
