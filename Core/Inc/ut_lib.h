/*
 * my_function.h
 *
 *  Created on: Jan 28, 2021
 *      Author: laksono
 */

#ifndef INC_UT_LIB_H_
#define INC_UT_LIB_H_

#include <stdint.h>

#define CS_TDC_Pin GPIO_PIN_12
#define CS_TDC_GPIO_Port GPIOE
#define START_TDC_Pin GPIO_PIN_13
#define START_TDC_GPIO_Port GPIOD
#define EN_TDC_Pin GPIO_PIN_15
#define EN_TDC_GPIO_Port GPIOD
#define INT_TDC_Pin GPIO_PIN_5
#define INT_TDC_GPIO_Port GPIOD
#define STOP_TDC_Pin GPIO_PIN_6
#define STOP_TDC_GPIO_Port GPIOD

typedef enum
{
	TIME_ECHO0_TO_ECHO1			= 0,
	TIME_ECHO1_TO_ECHO2,
	TIME_ECHO2_TO_ECHO3,
	TIME_ECHO3_TO_ECHO4
} time_tof_cal;

#define PULSE_TO_ECHO 0
#define ECHO_TO_ECHO 1

#define THICKNESS_VALUE 0
#define TIME_US_VALUE 1

void genPulse(const uint32_t msec, const uint8_t numStops);
float getMajorityElement( float *array, int size);
float rounding(float var);
int ambil_data_ut(float* ut_time,uint8_t num_tof,uint8_t num_stop);
float calibrating_ut(float *us_major);
float time_to_thick(float *time_us,uint16_t velo_material);

#endif /* INC_UT_LIB_H_ */
