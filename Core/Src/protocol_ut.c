/*
 * dci_ut_conf.c
 *
 *  Created on: Feb 11, 2021
 *      Author: laksono
 */

#include <stdint.h>
#include <protocol_ut.h>

#include "stm32l4xx_hal.h"
extern CRC_HandleTypeDef hcrc;

/****************************************************************
 * parse buff
 * get command & cek serial data receive
 *
 * IN
 * buf : serial data
 * size : size serial data buf
 *
 * OUT
 * kom_dat_ut: cmd dari HHU
 * return 0 is OK, 1 is crc not same
 *
 ***************************************************************/
status_parse parse_buff(uint8_t *buf,uint16_t size,struct cmd_ut *kom_dat_ut)
{

	#define PRE_HEADER_SIZE 5
	uint16_t offset_raw = 0;

	if(buf[0] == 0xFF && buf[1] == 0xFF && buf[2] == 0xFF
			&& buf[3] == 0xFF && buf[4] == 0xFF)
	{
		offset_raw += sizeof(uint8_t)*PRE_HEADER_SIZE;

		kom_dat_ut->command = buf[offset_raw];
		offset_raw += sizeof(kom_dat_ut->command);

		memcpy(&kom_dat_ut->byte_count,&buf[offset_raw],sizeof(kom_dat_ut->byte_count));
		offset_raw += sizeof(kom_dat_ut->byte_count);

		kom_dat_ut->status_code = buf[offset_raw];
		offset_raw += sizeof(kom_dat_ut->status_code);

		// disini di hiraukan dulu datanya
		offset_raw += kom_dat_ut->byte_count;

		// disini mustinya ada proces cek crc
		uint32_t len_crc = offset_raw;
		uint32_t hasil_crc_sendiri = HAL_CRC_Calculate(&hcrc,buf,len_crc);

		memcpy(&kom_dat_ut->crc_value,&buf[offset_raw],sizeof(kom_dat_ut->crc_value));
		offset_raw += sizeof(kom_dat_ut->crc_value);

		// disamakan dengan dari HHU
		// buffer tidak sama dengan yang dikirim musti minta ulangi
		if(hasil_crc_sendiri != kom_dat_ut->crc_value)
			return CRC_NOT_SAME;

		//akhirannya sudah di pastikan 13 /0xd / enter di awal
	}else return PRE_HEADER_NOT_VALID;

	return PARSE_OK;
}

/**
 * IN
 * kom_dat_ut: cmd balasan untukk HHU
 * dat: data yg mau di kirim
 *
 * OUT
 * resp_buff: buffer yang akan dikirim
 * size : size serial data buf
 *
 * return 0 ok , 1 ga dikenal
 *
 */
status_response response_cmd(struct cmd_ut *kom_dat_ut,uint8_t *resp_buff,uint16_t *byte_written)
{
	uint16_t offset_raw = 0;
	uint8_t cmd_not_recognized = 0;
	// cmd dari hhu
	uint8_t cmd_recv = kom_dat_ut->command;

	// pre-header 0xff 5 byte
	for(int x=0;x<PRE_HEADER_SIZE;x++)resp_buff[x]=0xFF;
	offset_raw += sizeof(uint8_t)*PRE_HEADER_SIZE;

	// balasan cmd buat hhu
	switch(cmd_recv){
	case DEVICE_CHECK_CMD:
		resp_buff[offset_raw] = DEVICE_STATUS_CMD;
		break;
	case SEND_DATA_CMD:
		resp_buff[offset_raw] = TAKE_DATA_CMD;
		break;
	case SEND_CONF_CMD:
		resp_buff[offset_raw] = RESPONSE_CONF_CMD;
		break;
	case READ_CONF_CMD:
		resp_buff[offset_raw] = RESPONSE_CONF_CMD;
		break;
	case GOTO_CAL_MODE_CMD:
		resp_buff[offset_raw] = RESP_CAL_MODE_CMD;
		break;
	case EXIT_CAL_MODE_CMD:
		resp_buff[offset_raw] = RESP_CAL_MODE_CMD;
		break;
	default:
		resp_buff[offset_raw] = NOT_FOUND_CMD;
		cmd_not_recognized = NOT_RECOGNIZE_CMD;
		break;
	}
	offset_raw += sizeof(uint8_t);

	// jika cmd ga di kenal hati2 kita force ke value ini
	if(cmd_not_recognized == NOT_RECOGNIZE_CMD){
		kom_dat_ut->byte_count = 0;
		kom_dat_ut->status_code = 1;
	}

	// isi nilai byte_count
	memcpy(&resp_buff[offset_raw],&kom_dat_ut->byte_count,sizeof(kom_dat_ut->byte_count));
	offset_raw += sizeof(kom_dat_ut->byte_count);

	// isi status code
	resp_buff[offset_raw]=kom_dat_ut->status_code;
	offset_raw += sizeof(kom_dat_ut->status_code);

	// data musti sudah di isi sebelumnya
	offset_raw += kom_dat_ut->byte_count;

	// disini harusnya melakukan crc
	uint32_t len_crc = offset_raw;
	kom_dat_ut->crc_value = HAL_CRC_Calculate(&hcrc,resp_buff,len_crc);

	// masukan hasil crc
	memcpy(&resp_buff[offset_raw],&kom_dat_ut->crc_value,sizeof(kom_dat_ut->crc_value));
	offset_raw += sizeof(kom_dat_ut->crc_value);

	// end of data
	resp_buff[offset_raw]=0xd;
	offset_raw += sizeof(uint8_t);

	*byte_written = offset_raw;

	if(cmd_not_recognized == NOT_RECOGNIZE_CMD)
		return NOT_RECOGNIZE_CMD;
	else
		return RESPONSE_OK;

}

void dummy_data(uint8_t *buff_data_ut,uint16_t start_data,uint16_t jum)
{
	struct data_ut dummy_data_ut;
	uint16_t offset_dummy = 0;

	dummy_data_ut.report_log= 1;
	for(int x=0;x<jum;x++){
		dummy_data_ut.time_stamp=1613392937+x;
		int y;
		for(y=0;y<6;y++)
			dummy_data_ut.hasil[y]=(float)y+start_data;

		memcpy(&buff_data_ut[offset_dummy],&dummy_data_ut,sizeof(dummy_data_ut));

		offset_dummy += sizeof(dummy_data_ut);
	}
}

/**
 * digunakan untuk membalas data byte_count >= 1
 */
status_balasan siapin_data_balasan(struct cmd_ut *kom_dat_ut,uint8_t *kom_buff)
{

	uint8_t report;

	if(kom_dat_ut->command == SEND_DATA_CMD){
		struct data_ut dat_ut;
		kom_dat_ut->byte_count = MAX_DATA_SEND*sizeof(dat_ut);
		// ambil data num brp mulai ambil?
		uint16_t data_num = kom_buff[OFFSET_DATA_BUFF];
		dummy_data(&kom_buff[OFFSET_DATA_BUFF],data_num,MAX_DATA_SEND);
		report = DATA_BALASAN_PREPARED;
	}
	else if(kom_dat_ut->command == SEND_CONF_CMD){
		struct setting_ut set_ut;
		// baca set dari HHU
		memcpy(&set_ut,&kom_buff[OFFSET_DATA_BUFF],kom_dat_ut->byte_count);
		// save ke flash mem
		W25QXX_Write((uint8_t *)&set_ut,0,sizeof(set_ut));
		// baca lg dr flash mem buat mastikan sudah di simpan
		W25QXX_Read((uint8_t *)&set_ut,0,sizeof(set_ut));
		// kasih tau hhu lagi set nya
		memcpy(&kom_buff[OFFSET_DATA_BUFF],&set_ut,kom_dat_ut->byte_count);
		report = DATA_BALASAN_PREPARED;
	}
	else if(kom_dat_ut->command == READ_CONF_CMD){
		struct setting_ut set_ut;
		// baca config dr flash mem
		W25QXX_Read((uint8_t *)&set_ut,0,sizeof(set_ut));
		// kasih tau hhu set nya
		kom_dat_ut->byte_count = sizeof(set_ut);
		memcpy(&kom_buff[OFFSET_DATA_BUFF],&set_ut,kom_dat_ut->byte_count);
		report = DATA_BALASAN_PREPARED;
	}
	else report = NO_NEED_DATA_BALASAN;

	return report;
}
