/*
 * epoch_time.h
 *
 *  Created on: Feb 19, 2021
 *      Author: laksono
 */

#ifndef INC_EPOCH_TIME_H_
#define INC_EPOCH_TIME_H_

#include <stdint.h>

struct DateTime
{
    uint16_t year;
    uint8_t month;
    uint8_t day;
    uint8_t dayOfWeek;
    uint8_t hours;
    uint8_t minutes;
    uint8_t seconds;
    uint16_t milliseconds;
};

uint8_t computeDayOfWeek(uint16_t y, uint8_t m, uint8_t d);
void convertUnixTimeToDate(unsigned long t, struct DateTime *date);


#endif /* INC_EPOCH_TIME_H_ */
