#include "io_expander.h"

#define bitSet(a,b)  a |= 1 << b
#define bitClear(a,b) a &= ~(1 << b)

extern I2C_HandleTypeDef hi2c1;

#define MCP23017_ADDRESS_21 0x21
#define I2C_TIMEOUT			10

void MCP23017_init()
{
	//BANK = 	0 : sequential register addresses
	//MIRROR = 	0 : use configureInterrupt
	//SEQOP = 	1 : sequential operation disabled, address pointer does not increment
	//DISSLW = 	0 : slew rate enabled
	//HAEN = 	0 : hardware address pin is always enabled on 23017
	//ODR = 	0 : open drain output
	//INTPOL = 	0 : interrupt active low
	MCP23017_writeRegister(REGISTER_IOCON, 0b00000000);

	//enable all pull up resistors (will be effective for input pins only)
	MCP23017_writeRegisters(REGISTER_GPPU_A, 0xFF, 0xFF);
}

void MCP23017_portMode(uint8_t port, uint8_t directions, uint8_t pullups, uint8_t inverted)
{
	MCP23017_writeRegister(REGISTER_IODIR_A + port, directions);
	MCP23017_writeRegister(REGISTER_GPPU_A + port, pullups);
	MCP23017_writeRegister(REGISTER_IPOL_A + port, inverted);
}

void MCP23017_pinMode(uint8_t pin, uint8_t mode, uint8_t inverted)
{
	uint8_t iodirreg = REGISTER_IODIR_A;
	uint8_t pullupreg = REGISTER_GPPU_A;
	uint8_t polreg = REGISTER_IPOL_A;
	uint8_t iodir, pol, pull;

	if(pin > 7)
	{
		iodirreg = REGISTER_IODIR_B;
		pullupreg = REGISTER_GPPU_B;
		polreg = REGISTER_IPOL_B;
		pin -= 8;
	}

	iodir = MCP23017_readRegister(iodirreg);
	if(mode == INPUT || mode == INPUT_PULLUP) bitSet(iodir, pin);
	else bitClear(iodir, pin);

	pull = MCP23017_readRegister(pullupreg);
	if(mode == INPUT_PULLUP) bitSet(pull, pin);
	else bitClear(pull, pin);

	pol = MCP23017_readRegister(polreg);
	if(inverted) bitSet(pol, pin);
	else bitClear(pol, pin);

	MCP23017_writeRegister(iodirreg, iodir);
	MCP23017_writeRegister(pullupreg, pull);
	MCP23017_writeRegister(polreg, pol);
}

void MCP23017_digitalWrite(uint8_t pin, uint8_t state)
{
	uint8_t gpioreg = REGISTER_GPIO_A;
	uint8_t gpio;
	if(pin > 7)
	{
		gpioreg = REGISTER_GPIO_B;
		pin -= 8;
	}

	gpio = MCP23017_readRegister(gpioreg);

	if(state == 1) bitSet(gpio, pin);
	else bitClear(gpio, pin);

	MCP23017_writeRegister(gpioreg, gpio);
}

void register_Write(uint8_t *port,uint8_t pin, uint8_t state)
{
	uint8_t data,pinB_selected = 0;

	if(pin > 7){
		data = port[1];
		pin -= 8;
		pinB_selected = 1;
	}
	else
		data = port[0];

	if(state == 1) bitSet(data, pin);
	else bitClear(data, pin);

	if(pinB_selected == 1)
		port[1] = data;
	else
		port[0] = data;
}


uint8_t MCP23017_digitalRead(uint8_t pin)
{
	uint8_t gpioreg = REGISTER_GPIO_A;
	uint8_t gpio;
	if(pin > 7)
	{
		gpioreg = REGISTER_GPIO_B;
		pin -=8;
	}

	gpio = readRegister(gpioreg);
	if(bitRead(gpio, pin)) return 1;
	return 0;
}

void MCP23017_writePort(uint8_t port, uint8_t value)
{
	MCP23017_writeRegister(REGISTER_GPIO_A + port, value);
}

void MCP23017_writePorts(uint8_t *value){
	HAL_I2C_Mem_Write(&hi2c1, MCP23017_ADDRESS_21<<1, REGISTER_GPIO_A, 1, value, 2, I2C_TIMEOUT);
}

void MCP23017_readPorts(uint8_t *value){
	HAL_I2C_Mem_Read(&hi2c1, MCP23017_ADDRESS_21<<1, REGISTER_GPIO_A, 1, value, 2, I2C_TIMEOUT);
}

uint8_t MCP23017_readPort(uint8_t port)
{
	return MCP23017_readRegister(REGISTER_GPIO_A + port);
}

uint16_t MCP23017_read()
{
	uint8_t a = MCP23017_readPort(0);
	uint8_t b = MCP23017_readPort(1);

	return a | b << 8;
}

void MCP23017_writeRegister(uint16_t reg, uint8_t value)
{
	uint8_t status;
	uint8_t data[1];
	data[0]=  value;
	status = HAL_I2C_Mem_Write(&hi2c1, MCP23017_ADDRESS_21<<1, reg, 1, data, 1, I2C_TIMEOUT);

}

void MCP23017_writeRegisters(uint8_t reg, uint8_t portA, uint8_t portB)
{
	HAL_I2C_Mem_Write(&hi2c1, MCP23017_ADDRESS_21<<1, reg, 1, &portA, 1, I2C_TIMEOUT);
	HAL_I2C_Mem_Write(&hi2c1, MCP23017_ADDRESS_21<<1, reg+1, 1, &portB, 1, I2C_TIMEOUT);
}


uint8_t MCP23017_readRegister(uint8_t reg)
{
	uint8_t data;
	HAL_I2C_Mem_Read(&hi2c1, MCP23017_ADDRESS_21<<1, reg, 1, &data, 1, I2C_TIMEOUT);

	return data;
}

void MCP23017_readRegisters(uint8_t reg, uint8_t *portA, uint8_t *portB)
{
	uint8_t data;
	HAL_I2C_Mem_Read(&hi2c1, MCP23017_ADDRESS_21<<1, reg, 1, &data, 1, I2C_TIMEOUT);
	portA = data;
	HAL_I2C_Mem_Read(&hi2c1, MCP23017_ADDRESS_21<<1, reg+1, 1, &data, 1, I2C_TIMEOUT);
	portB = data;
}

#ifdef _MCP23017_INTERRUPT_SUPPORT_

void MCP23017_interruptMode(uint8_t intMode)
{
	uint8_t iocon = MCP23017_readRegister(REGISTER_IOCON);
	if(intMode == Or) iocon |= (uint8_t)Or;
	else iocon &= ~(uint8_t)Or;

	MCP23017_writeRegister(REGISTER_IOCON, iocon);
}

void MCP23017_interrupt_pin(uint8_t port, uint8_t pin, uint8_t mode)
{
	uint8_t defvalreg = REGISTER_DEFVAL_A + port;
	uint8_t intconreg = REGISTER_INTCON_A + port;

	//enable interrupt for port
	MCP23017_writeRegister(REGISTER_GPINTEN_A + port, pin);
	switch(mode)
	{
	case CHANGE:
		//interrupt on change
		MCP23017_writeRegister(intconreg, 0);
		break;
	case FALLING:
		//interrupt falling : compared against defval, 0xff
		MCP23017_writeRegister(intconreg, pin);
		MCP23017_writeRegister(defvalreg, pin);
		break;
	case RISING:
		//interrupt rising : compared against defval, 0x00
		MCP23017_writeRegister(intconreg, pin);
		MCP23017_writeRegister(defvalreg, 0x00);
		break;
	}
}

void MCP23017_interrupt(uint8_t port, uint8_t mode)
{
	uint8_t defvalreg = REGISTER_DEFVAL_A + port;
	uint8_t intconreg = REGISTER_INTCON_A + port;

	//enable interrupt for port
	MCP23017_writeRegister(REGISTER_GPINTEN_A + port, 0xFF);
	switch(mode)
	{
	case CHANGE:
		//interrupt on change
		MCP23017_writeRegister(intconreg, 0);
		break;
	case FALLING:
		//interrupt falling : compared against defval, 0xff
		MCP23017_writeRegister(intconreg, 0xFF);
		MCP23017_writeRegister(defvalreg, 0xFF);
		break;
	case RISING:
		//interrupt rising : compared against defval, 0x00
		MCP23017_writeRegister(intconreg, 0xFF);
		MCP23017_writeRegister(defvalreg, 0x00);
		break;
	}
}

void MCP23017_interruptedBy(uint8_t *portA, uint8_t *portB)
{
	*portA = MCP23017_readRegister(REGISTER_INTF_A);
	*portB = MCP23017_readRegister(REGISTER_INTF_B);
}

void MCP23017_disableInterrupt(uint8_t port)
{
	MCP23017_writeRegister(REGISTER_GPINTEN_A + port, 0x00);
}

void MCP23017_clearInterrupts_all()
{
	uint8_t a, b;
	MCP23017_clearInterrupts(a, b);
}

void MCP23017_clearInterrupts(uint8_t *portA, uint8_t *portB)
{
	*portA = MCP23017_readRegister(REGISTER_INTCAP_A);
	*portB = MCP23017_readRegister(REGISTER_INTCAP_B);
}


#endif
