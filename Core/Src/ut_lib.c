/*
 * my_function.c
 *
 *  Created on: Jan 28, 2021
 *      Author: laksono
 */
#include "ut_lib.h"
#include "stm32l4xx_hal.h"

void genPulse(const uint32_t msec, const uint8_t numStops)
{
    HAL_GPIO_WritePin(GPIOD, START_TDC_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOD, START_TDC_Pin, GPIO_PIN_RESET);

    for (uint8_t i = 0; i < numStops; ++i)
    {
        HAL_Delay(msec);

        HAL_GPIO_WritePin(GPIOD, STOP_TDC_Pin, GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOD, STOP_TDC_Pin, GPIO_PIN_RESET);
    }
}


float getMajorityElement( float *array, int size) {
 int i, majorityIndex = 0, count = 1;
	float valuetmp=0;
    /* Find Majority Element */
    for(i = 1; i < size; i++) {
     /* Check if current element is same as majority element,
				If yes then increment count otherwise decrement count */
        if(array[majorityIndex] == array[i])
            count++;
        else
            count--;

        if(count == 0) {
            majorityIndex = i;
            count = 1;
        }
    }
    /* Verify, If array[majorityIndex] is the majority element */
    count = 0;
    /* Count the frequency of array[majorityIndex] in array */
    for (i = 0; i < size; i++) {
        if(array[i] == array[majorityIndex])
            count++;
		}

	 /* Check if count of majority element is more than size/2,
	 If yes, then it is a majority element otherwise not  */
    if(count > (size/2)){
        #ifdef DEBUG_TXRX
				uprintf("Majority Element : %d\n", array[majorityIndex]);
				#endif
			valuetmp = array[majorityIndex];
			return valuetmp;

	  }else{
        #ifdef DEBUG_TXRX
				uprintf("No Majority Element Found, try to measure again\n");
				#endif
			//reset_capture(NULL,NULL);
				valuetmp = 0.00;
			return valuetmp;
		}
}

float rounding(float var)
{
    // 37.66666 * 100 =3766.66
    // 3766.66 + .5 =3767.16    for rounding off value
    // then type cast to int so value is 3767
    // then divided by 100 so the value converted into 37.67
    float value = (int)(var * 100 + .5);
	value /= 100;
    return value;
}

int ambil_data_ut(float* ut_time,uint8_t num_tof,uint8_t num_stop){

	#define MEASURE_TIMEOUT -1
	#define MEASURE_FAILED -2
	#define MODE_FALSE -3
	#define MEASURE_OK 0

	float tof_time[5];
	float echo_time;
	TDC7200_startMeasurement();

	HAL_GPIO_TogglePin(GPIOE, GPIO_PIN_5);

	HAL_GPIO_WritePin(EN_TDC_GPIO_Port, EN_TDC_Pin, GPIO_PIN_RESET);

	// 100 us start pulse
	for(int y =0;y<100;y++);

	HAL_GPIO_WritePin(EN_TDC_GPIO_Port, EN_TDC_Pin, GPIO_PIN_SET);

	// timeout 100 ms nunggu tdc int
	int timeout_skip = 100;
//	uprintf("wait int...");
	while(HAL_GPIO_ReadPin(INT_TDC_GPIO_Port,INT_TDC_Pin) == 1){
		timeout_skip--;
		if(timeout_skip == 0)break;
		HAL_Delay(1);
	}
//	uprintf("[OK]\r\n");

	if(timeout_skip == 0)
		return MEASURE_TIMEOUT;

	for (uint8_t stop = 1; stop <= num_stop; ++stop)
	{
		uint64_t time;
		uint8_t status_measurement =TDC7200_readMeasurement(stop, &time);
		if (status_measurement)
			tof_time[stop-1]=(float)time/(float)1000000;
		else
			return MEASURE_FAILED;
	}

	switch(num_tof){
	case 0:
		echo_time = tof_time[1]-tof_time[0];
		break;
	case 1:
		echo_time = tof_time[2]-tof_time[1];
		break;
	case 2:
		echo_time = tof_time[3]-tof_time[2];
		break;
	case 3:
		echo_time = tof_time[4]-tof_time[3];
		break;
	}

	*ut_time = echo_time;

	// ut istirahat 1 ms
	HAL_Delay(5);

	return 0;
}

/**
 * How To Cal:
 * - baca echo-echo cal block 5mm
 * - catat nilai us time
 * - masukan ke excel
 */
float calibrating_ut(float *us_major){

	#define us_osci_in_5mm (float)1.689189

	float us_tdc = *us_major;
	float delta_us = us_osci_in_5mm - us_tdc;

	return delta_us;
}

float time_to_thick(float *time_us,uint16_t velo_material){
	float us_time = *time_us;
	float thickness_material = (us_time/2.0)*((float)velo_material/1000.0);
	return thickness_material;
}

int main_take_data(float *data_hasil,float *offset_val,uint8_t major_value){
	float timing_us[50];
	uint8_t pulse_mode = ECHO_TO_ECHO;
	#define STOP_NUMBER 5

	  for(int x=0;x<major_value;x++){
		ambil_data_ut(&timing_us[x],TIME_ECHO0_TO_ECHO1,STOP_NUMBER);
		timing_us[x] = rounding(timing_us[x]);
		#ifdef DEBUG_MEASURE
			uprintf("[%d] = %f us\r\n",x,timing_us[x]);
		#endif
	  }
	  #ifdef DEBUG_MEASURE
		  uprintf("=================\r\n");
	  #endif

	  float times_ut = getMajorityElement(timing_us,major_value);

	  float us_offset = *offset_val;
	  float time_after_offset = times_ut + us_offset;
	  #ifdef DEBUG_MEASURE
	  //	if(times_ut != 0)
		  uprintf("%f us >>>> cal %f us\r\n",times_ut,rounding(time_after_offset));

		  uprintf("=================\r\n");
	  #endif

	  *data_hasil = rounding(time_after_offset);

	return 0;
}
