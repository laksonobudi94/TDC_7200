/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdarg.h>
#include "tdc7200.h"
#include "ut_lib.h"
#include "u8g2.h"
#include "io_expander.h"
#include "reset_status.h"
#include "octospi.h"
#include "protocol_ut.h"
#include "float.h"
#include "math.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CRC_HandleTypeDef hcrc;

I2C_HandleTypeDef hi2c1;

IWDG_HandleTypeDef hiwdg;

OSPI_HandleTypeDef hospi1;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;

TIM_HandleTypeDef htim4;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */

float ut_ch_data[6];int inc=0;

uint8_t rx_data;
uint8_t rx_index = 0;
uint8_t rx_buffer[5000];
struct cmd_ut komu_data;

uint8_t change_ui_flag;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM4_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_IWDG_Init(void);
static void MX_SPI2_Init(void);
static void MX_I2C1_Init(void);
static void MX_OCTOSPI1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_CRC_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
u8g2_t u8g2;

uint8_t u8x8_stm32_gpio_and_delay(U8X8_UNUSED u8x8_t *u8x8,
    U8X8_UNUSED uint8_t msg, U8X8_UNUSED uint8_t arg_int,
    U8X8_UNUSED void *arg_ptr)
{
  switch (msg)
  {
  case U8X8_MSG_GPIO_AND_DELAY_INIT:
//    HAL_Delay(100);
    break;
  case U8X8_MSG_DELAY_MILLI:
//    HAL_Delay(arg_int);
    break;
  case U8X8_MSG_GPIO_DC:
	  //A0 pin
	  MCP23017_digitalWrite(PIN_A0,arg_int);
    break;
  case U8X8_MSG_GPIO_RESET:
	  MCP23017_digitalWrite(PIN_RST,arg_int);
	break;
  }
  return 1;
}


uint8_t io_exp_state[2];
void cmd_lcd(uint8_t b)
{
	uint8_t i;

    MCP23017_readPorts(io_exp_state);

    register_Write(io_exp_state,PIN_A0,1);
    register_Write(io_exp_state,PIN_RW,1);
    register_Write(io_exp_state,PIN_E,1);
    MCP23017_writePorts(io_exp_state);

    for( i = U8X8_MSG_GPIO_D0; i <= U8X8_MSG_GPIO_D7; i++ )
	{
	  switch(i){
	  case U8X8_MSG_GPIO_D0:
		  register_Write(io_exp_state,PIN_A4, b&1);
		  break;
	  case U8X8_MSG_GPIO_D1:
		  register_Write(io_exp_state,PIN_A5, b&1);
		  break;
	  case U8X8_MSG_GPIO_D2:
		  register_Write(io_exp_state,PIN_A6, b&1);
		  break;
	  case U8X8_MSG_GPIO_D3:
		  register_Write(io_exp_state,PIN_A7, b&1);
		  break;
	  case U8X8_MSG_GPIO_D4:
		  register_Write(io_exp_state,PIN_B7, b&1);
		  break;
	  case U8X8_MSG_GPIO_D5:
		  register_Write(io_exp_state,PIN_B6, b&1);
		  break;
	  case U8X8_MSG_GPIO_D6:
		  register_Write(io_exp_state,PIN_B5, b&1);
		  break;
	  case U8X8_MSG_GPIO_D7:
		  register_Write(io_exp_state,PIN_B4, b&1);
		  break;
	  default:
		  break;
	  }
	  b >>= 1;
	}

    register_Write(io_exp_state,PIN_A0, 0);
    register_Write(io_exp_state,PIN_RW, 0);

    register_Write(io_exp_state,PIN_E,1);
    MCP23017_writePorts(io_exp_state);
	MCP23017_digitalWrite(PIN_E,0);
}

uint8_t u8x8_byte_8bit_6800mode(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
  uint8_t i, b;
  uint8_t *data;

  switch(msg)
  {
    case U8X8_MSG_BYTE_SEND:
      data = (uint8_t *)arg_ptr;

      MCP23017_readPorts(io_exp_state);

      while( arg_int > 0 )
      {
		b = *data;
		data++;
		arg_int--;

		int hitung=0;
		for( i = U8X8_MSG_GPIO_D0; i <= U8X8_MSG_GPIO_D7; i++ )
		{
		  switch(i){
		  case U8X8_MSG_GPIO_D0:
			  register_Write(io_exp_state,PIN_A4, b&1);
			  break;
		  case U8X8_MSG_GPIO_D1:
			  register_Write(io_exp_state,PIN_A5, b&1);
			  break;
		  case U8X8_MSG_GPIO_D2:
			  register_Write(io_exp_state,PIN_A6, b&1);
			  break;
		  case U8X8_MSG_GPIO_D3:
			  register_Write(io_exp_state,PIN_A7, b&1);
			  break;
		  case U8X8_MSG_GPIO_D4:
			  register_Write(io_exp_state,PIN_B7, b&1);
			  break;
		  case U8X8_MSG_GPIO_D5:
			  register_Write(io_exp_state,PIN_B6, b&1);
			  break;
		  case U8X8_MSG_GPIO_D6:
			  register_Write(io_exp_state,PIN_B5, b&1);
			  break;
		  case U8X8_MSG_GPIO_D7:
			  register_Write(io_exp_state,PIN_B4, b&1);
			  break;
		  default:
			  break;
		  }
		  b >>= 1;
		}

	    register_Write(io_exp_state,PIN_E,1);
        MCP23017_writePorts(io_exp_state);
		MCP23017_digitalWrite(PIN_E,0);
      }
      break;

    case U8X8_MSG_BYTE_INIT:
    	/* disable chipselect */
    	// E
    	MCP23017_digitalWrite(PIN_E,0);
      break;
    case U8X8_MSG_BYTE_SET_DC:
		MCP23017_digitalWrite(PIN_A0,arg_int);
      break;
    case U8X8_MSG_BYTE_START_TRANSFER:

      break;
    case U8X8_MSG_BYTE_END_TRANSFER:

    	break;
    default:
      return 0;
  }
  return 1;
}

// reference:
// https://www.youtube.com/watch?v=VdHt_wJdezM
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance==USART1)
	{
		// if the data is not being received, clear the buffer
		if(rx_index==0)
		{
			for(int i=0;i<20;i++)
			{
				rx_buffer[i]=0;
			}
		}
		// if the character received is other than 'enter' ascii13, save the data in buffer
		if(rx_data!=13)
		{
			rx_buffer[rx_index++]=rx_data;
		}
		else
		{
			uint16_t byte_send;

			uint8_t cek = parse_buff(rx_buffer,rx_index,&komu_data);

			siapin_data_balasan(&komu_data,rx_buffer);

			if(komu_data.command == GOTO_CAL_MODE_CMD ||
					komu_data.command == EXIT_CAL_MODE_CMD ||
					komu_data.command == SAVE_CAL_VAL_CMD)
			{
				change_ui_flag = 1;
			}

			response_cmd(&komu_data,rx_buffer,&byte_send);
			rx_index=0;

			HAL_UART_Transmit(&huart1,rx_buffer,byte_send,20000);// transmit the data via uart

			if(change_ui_flag == 1){
				if(komu_data.command == GOTO_CAL_MODE_CMD){
					state_gui = CAL_SCREEN;
					cmd_lcd(0xAE);
					u8g2_ClearDisplay(&u8g2);
					view_calibrate(0.0);
					cmd_lcd(0xAF);
				}
				else if(komu_data.command == EXIT_CAL_MODE_CMD){
					state_gui = MAIN_SCREEN;
					cmd_lcd(0xAE);
					u8g2_ClearDisplay(&u8g2);
					view_ut_data(ut_ch_data,inc);
					cmd_lcd(0xAF);
				}

//				change_ui_flag = 0;
//				memset(&komu_data,0,sizeof(komu_data));
			}
		}
		HAL_UART_Receive_IT(&huart1,&rx_data,1);// receive data (one character only)
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_TIM4_Init();
  MX_USART3_UART_Init();
//  MX_IWDG_Init();
  MX_SPI2_Init();
  MX_I2C1_Init();
  MX_OCTOSPI1_Init();
  MX_USART1_UART_Init();
  MX_CRC_Init();
  /* USER CODE BEGIN 2 */

  //	__HAL_UART_ENABLE_IT(&huart1,UART_IT_RXNE);
  	HAL_UART_Receive_IT(&huart1,&rx_data,1);

  printf("\r\n");
  printf("\r\n");
  printf("\r\n");
  printf("==========================\r\n");
  printf("FW UT v2.0.0\r\n");
  printf("==========================\r\n");

  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);
//  htim4.Instance->CCR1 = 50;
  reset_cause_t reset_cause = reset_cause_get();
  printf("The system reset cause is \"%s\"\r\n", reset_cause_get_name(reset_cause));
  printf("22 Khz Must be run...\r\n");

  #define TDC7200_CLOCK_FREQ_HZ (8000000)

  TDC7200(TDC7200_CLOCK_FREQ_HZ);
  printf("TDC7200_begin..\r\n");
  uint8_t status_tdc = TDC7200_begin();
  if(!status_tdc){
	  printf("TDC7200_begin Failed!\r\n");
	  Error_Handler();
  }
  else
	  printf("TDC7200_begin Success!\r\n");
#define STOP_NUMBER 5
  if (!TDC7200_setupMeasurement( 	10,         // cal2Periods
                                    1,          // avgCycles
									STOP_NUMBER,  // numStops
                                    2 ))        // mode
  {
	  printf("TDC7200_setupMeasurement Failed!\r\n");
	  Error_Handler();
  }
  else
	  printf("TDC7200_setupMeasurement Success!\r\n");


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  HAL_GPIO_WritePin(GPIOD, START_TDC_Pin, GPIO_PIN_SET); //buat start tdc
  HAL_GPIO_WritePin(GPIOD, RST_MCP_Pin, GPIO_PIN_RESET);
  HAL_Delay(100);
  HAL_GPIO_WritePin(GPIOD, RST_MCP_Pin, GPIO_PIN_SET);

  uint8_t status;
  MCP23017_init();
  MCP23017_portMode(0, 0, 0xFF, 0);//PA
  //B0-B2 is input, other is output
  MCP23017_portMode(1, 0b00000111, 0xFF, 0);//PB

  HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
  MCP23017_interruptMode(Separated);

  MCP23017_interrupt_pin(1, 0b00000111, FALLING);

  MCP23017_writeRegister(REGISTER_IPOL_A,0);
  MCP23017_writeRegister(REGISTER_IPOL_B,0);

  MCP23017_writeRegister(REGISTER_GPIO_A,0);
  MCP23017_writeRegister(REGISTER_GPIO_B,0b00000111);
  MCP23017_clearInterrupts_all();
  printf("MCP iodir set...\r\n");

  // Init LCD
  MCP23017_digitalWrite(PIN_BLA,1);
  u8g2_Setup_st7565_64128n_f(&u8g2, U8G2_R0, u8x8_byte_8bit_6800mode, u8x8_stm32_gpio_and_delay);
  u8g2_InitDisplay(&u8g2); // send init sequence to the display, display is in sleep mode after this,
  u8g2_SetPowerSave(&u8g2, 0); // wake up display
  u8g2_SetFlipMode(&u8g2,1);
  u8g2_SetContrast(&u8g2,20);
  u8g2_ClearDisplay(&u8g2);
  u8g2_FirstPage(&u8g2);
  tampil_init();
  u8g2_UpdateDisplay(&u8g2);
  printf("LCD init done...\r\n");

  state_gui = MAIN_SCREEN;
  uint8_t 	 baris = 1;

// hati2 timeout!
// HAL_Delay(300);
// HAL_IWDG_Refresh(&hiwdg);


// 200 us
// MCP23017_digitalWrite(PIN_B2,1);
// MCP23017_digitalWrite(PIN_B2,0);
// HAL_Delay(100);

// kalo mw cepet pake ini
// SEQOP = 	1 : sequential operation disabled, address pointer does not increment
// MCP23017_writeRegister(REGISTER_IOCON, 0b00100000);
// HAL_I2C_Mem_Write(&hi2c1, MCP23017_ADDRESS_21<<1, reg, 1, &portA, 2, I2C_TIMEOUT);
//
// MCP23017_writeRegister(REGISTER_IOCON, 0b00000000);



  #define BACK_TO_MAIN_SETTING baris = 0;set_menu(baris);state_gui = SET_SCREEN

  uint8_t init_status = 0;
  printf("QSPI Init...\r\n");
  init_status = CSP_QUADSPI_Init();
  if(init_status){
	  printf("QSPI Init Failed...\r\n");
	  // fail in here
	  Error_Handler();
  }
  printf("QSPI Init OK...\r\n");

#if 0
	#define LEN_TEST 3000
	uint8_t crcArray[LEN_TEST];
	for(int x=0;x<LEN_TEST;x++){
		crcArray[x]=x;
	}

	//CRC-32/MPEG-2
	//https://crccalc.com/
	uint32_t crcVal = HAL_CRC_Calculate(&hcrc,crcArray,LEN_TEST);
#endif


  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
//  #define DEBUG_MEASURE
  view_ut_data(ut_ch_data,inc);

  struct cal_ut cal_val_ut;
  struct setting_ut config_ut;
  float thickness,time_us;

  W25QXX_Read((uint8_t *)&cal_val_ut,sizeof(config_ut),sizeof(cal_val_ut));

//  memset((uint8_t *)&cal_val_ut,0xFFFFFFFF,sizeof(cal_val_ut));
  float cal_offset_time_us = cal_val_ut.offset_time_us_sensoring;
  // detect 0xFFFFFFFF in flash mem
  if(cal_offset_time_us >= FLT_MAX || isnan(cal_offset_time_us))
	  cal_offset_time_us=0.0;
  printf("cal_offset_time_us %f\r\n",cal_offset_time_us);

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

		if(change_ui_flag == 1){
			if(komu_data.command == SAVE_CAL_VAL_CMD){
				  state_gui = SAVE_CALIBRATED_SCREEN;
				  main_take_data(&time_us,NULL,MAJORITY_VALUE);
				  cal_offset_time_us = calibrating_ut(&time_us);
				  main_take_data(&time_us,&cal_offset_time_us,MAJORITY_VALUE);
				  thickness = time_to_thick(&time_us,VELO_STEEL_MILD);
				  view_saved_calibrate(&thickness);
				  W25QXX_Read((uint8_t *)&cal_val_ut,sizeof(config_ut),sizeof(cal_val_ut));
				  cal_val_ut.offset_time_us_sensoring = cal_offset_time_us;
				  W25QXX_Write((uint8_t *)&cal_val_ut,sizeof(config_ut),sizeof(cal_val_ut));
				  W25QXX_Read((uint8_t *)&cal_val_ut,sizeof(config_ut),sizeof(cal_val_ut));
				  printf("cal_val_ut.offset_time_us_sensoring %f\r\n",cal_val_ut.offset_time_us_sensoring);
			}
			change_ui_flag = 0;
			memset(&komu_data,0,sizeof(komu_data));
		}

	  if(button_pressed != NO_BUTTON_PRESSED){
		  // pada saat sedang draw hiraukan dulu int mcp
		  HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);

		  switch(state_gui){
		  case MAIN_SCREEN:
			  if(button_pressed == B2_PRESSED){
				  BACK_TO_MAIN_SETTING;
			  }
			  else {}
			  break;
		  case SET_SCREEN:
			  if(button_pressed == B2_PRESSED){
				  if(baris == 0){view_ut_data(ut_ch_data,inc);state_gui = MAIN_SCREEN;}
				  else if(baris == 1){
					  W25QXX_Read((uint8_t *)&config_ut,0,sizeof(config_ut));
					  view_look_config(config_ut,0);
					  state_gui = VIEW_CONF_SCREEN;
				  }
				  else if(baris == 2){view_calibrate(0.0);state_gui = CAL_SCREEN;}
				  else if(baris == 3){view_fw_info();state_gui = FW_INFO_SCREEN;}
				  else {}
			  }
			  else if(button_pressed == B1_PRESSED){
				  if(baris < 4)baris++;
				  else baris = 0;
				  set_menu(baris);
			  }
			  else if(button_pressed == B0_PRESSED){
				  if(baris > 0)baris--;
				  else baris = 4;
				  set_menu(baris);
			  }
			  else {}
			  break;
		  case VIEW_CONF_SCREEN:
			  if(button_pressed == B2_PRESSED){
				  BACK_TO_MAIN_SETTING;
			  }
			  else if(button_pressed == B0_PRESSED){
				  view_look_config(config_ut,1);
			  }
			  else if(button_pressed == B1_PRESSED){
				  view_look_config(config_ut,0);
			  }
			  else{}
			  break;
		  case CAL_SCREEN:
			  if(button_pressed == B2_PRESSED){

			  }
			  else if(button_pressed == B1_PRESSED){
				  BACK_TO_MAIN_SETTING;
			  }
			  else{}
			  break;
		  case FW_INFO_SCREEN:
			  if(button_pressed == B2_PRESSED){
				  BACK_TO_MAIN_SETTING;
			  }
			  else{}
			  break;
		  default:
			  break;
		  }
		  button_pressed = NO_BUTTON_PRESSED;
		  // jika sudah selesai draw boleh perhatikan int mcp
		  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
	  }
	  else{
		  switch(state_gui){
		  	  case MAIN_SCREEN:
		  		  for(int ch_x=0;ch_x<NUM_CH_UT;ch_x++){
		  			  main_take_data(&ut_ch_data[ch_x],&cal_offset_time_us,MAJORITY_VALUE);
		  			  ut_ch_data[ch_x] = time_to_thick(&ut_ch_data[ch_x],VELO_STEEL_MILD);
		  		  }
		  		  //hati2 disini bisa jadi kena int uart1
				  if(state_gui == MAIN_SCREEN)view_ut_data(ut_ch_data,inc);
				  if(inc<4000)inc++;
			  break;
		  case CAL_SCREEN:
			  if(button_pressed == NO_BUTTON_PRESSED){
	  			  main_take_data(&time_us,&cal_offset_time_us,MAJORITY_VALUE);
	  			  printf("cal_offset_time_us = %f\r\n",cal_offset_time_us);
	  			  thickness = time_to_thick(&time_us,VELO_STEEL_MILD);

				  //hati2 disini bisa jadi kena int uart1
				  if(state_gui == CAL_SCREEN)view_calibrate(&thickness);
			  }
			  break;
		  case SAVE_CALIBRATED_SCREEN:
			  main_take_data(&time_us,&cal_offset_time_us,MAJORITY_VALUE);
			  thickness = time_to_thick(&time_us,VELO_STEEL_MILD);
//			  printf("cal_offset_time_us = %f\r\n",cal_offset_time_us);
     		  if(state_gui == SAVE_CALIBRATED_SCREEN)view_saved_calibrate(&thickness);

			  break;
		  default:
			  break;
		  }
	  }
  }

  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 16;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART3
                              |RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_OSPI;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInit.OspiClockSelection = RCC_OSPICLKSOURCE_SYSCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_RCC_MCOConfig(RCC_MCO1, RCC_MCO1SOURCE_PLLCLK, RCC_MCODIV_4);
}

/**
  * @brief CRC Initialization Function
  * @param None
  * @retval None
  */
static void MX_CRC_Init(void)
{

  /* USER CODE BEGIN CRC_Init 0 */

  /* USER CODE END CRC_Init 0 */

  /* USER CODE BEGIN CRC_Init 1 */

  /* USER CODE END CRC_Init 1 */
  hcrc.Instance = CRC;
  hcrc.Init.DefaultPolynomialUse = DEFAULT_POLYNOMIAL_ENABLE;
  hcrc.Init.DefaultInitValueUse = DEFAULT_INIT_VALUE_ENABLE;
  hcrc.Init.InputDataInversionMode = CRC_INPUTDATA_INVERSION_NONE;
  hcrc.Init.OutputDataInversionMode = CRC_OUTPUTDATA_INVERSION_DISABLE;
  hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_BYTES;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CRC_Init 2 */

  /* USER CODE END CRC_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00300F38;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief IWDG Initialization Function
  * @param None
  * @retval None
  */
static void MX_IWDG_Init(void)
{

  /* USER CODE BEGIN IWDG_Init 0 */

  /* USER CODE END IWDG_Init 0 */

  /* USER CODE BEGIN IWDG_Init 1 */

  /* USER CODE END IWDG_Init 1 */
  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_256;
  hiwdg.Init.Window = 4095;
  hiwdg.Init.Reload = 4095;
  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN IWDG_Init 2 */

  /* USER CODE END IWDG_Init 2 */

}

/**
  * @brief OCTOSPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_OCTOSPI1_Init(void)
{

  /* USER CODE BEGIN OCTOSPI1_Init 0 */

  /* USER CODE END OCTOSPI1_Init 0 */

  OSPIM_CfgTypeDef OSPIM_Cfg_Struct = {0};

  /* USER CODE BEGIN OCTOSPI1_Init 1 */

  /* USER CODE END OCTOSPI1_Init 1 */
  /* OCTOSPI1 parameter configuration*/
  hospi1.Instance = OCTOSPI1;
  hospi1.Init.FifoThreshold = 4;
  hospi1.Init.DualQuad = HAL_OSPI_DUALQUAD_DISABLE;
  hospi1.Init.MemoryType = HAL_OSPI_MEMTYPE_MICRON;
  hospi1.Init.DeviceSize = 23;
  hospi1.Init.ChipSelectHighTime = 2;
  hospi1.Init.FreeRunningClock = HAL_OSPI_FREERUNCLK_DISABLE;
  hospi1.Init.ClockMode = HAL_OSPI_CLOCK_MODE_0;
  hospi1.Init.WrapSize = HAL_OSPI_WRAP_NOT_SUPPORTED;
  hospi1.Init.ClockPrescaler = 2;
  hospi1.Init.SampleShifting = HAL_OSPI_SAMPLE_SHIFTING_NONE;
  hospi1.Init.DelayHoldQuarterCycle = HAL_OSPI_DHQC_DISABLE;
  hospi1.Init.ChipSelectBoundary = 0;
  if (HAL_OSPI_Init(&hospi1) != HAL_OK)
  {
    Error_Handler();
  }
  OSPIM_Cfg_Struct.ClkPort = 1;
  OSPIM_Cfg_Struct.NCSPort = 1;
  OSPIM_Cfg_Struct.IOLowPort = HAL_OSPIM_IOPORT_1_LOW;
  if (HAL_OSPIM_Config(&hospi1, &OSPIM_Cfg_Struct, HAL_OSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN OCTOSPI1_Init 2 */

  /* USER CODE END OCTOSPI1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 10-1;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 39-1;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 6-1;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart1, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart1, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart3, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart3, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CS_TDC_GPIO_Port, CS_TDC_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LCD_BL_Pin|LCD_PWR_Pin|LCD_A0_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, RST_MCP_Pin|GPIO_PIN_15|STOP_TDC_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PE4 PE5 PE6 CS_TDC_Pin */
  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|CS_TDC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : HHU_CONNECT_Pin */
  GPIO_InitStruct.Pin = HHU_CONNECT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(HHU_CONNECT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LCD_BL_Pin LCD_PWR_Pin LCD_A0_Pin */
  GPIO_InitStruct.Pin = LCD_BL_Pin|LCD_PWR_Pin|LCD_A0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : RST_MCP_Pin PD15 STOP_TDC_Pin */
  GPIO_InitStruct.Pin = RST_MCP_Pin|GPIO_PIN_15|STOP_TDC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PA8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF0_MCO;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : INT_TDC_Pin */
  GPIO_InitStruct.Pin = INT_TDC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(INT_TDC_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : INT_PB_MCP_Pin */
  GPIO_InitStruct.Pin = INT_PB_MCP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(INT_PB_MCP_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */
int _write(int file, char *ptr, int len)
{
	HAL_UART_Transmit(&huart3,ptr,len,1000);
	return len;
}
// EXTI Line9 External Interrupt ISR Handler CallBackFun
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GPIO_PIN_7)
    {
		// biar ga ada interrupt susulan
    	portB = MCP23017_readPort(1);

    	if(portB & 1 << 0)pin_B0 = 1;
    	else pin_B0 = 0;
    	if(portB & 1 << 1)pin_B1 = 1;
    	else pin_B1 = 0;
    	if(portB & 1 << 2)pin_B2 = 1;
    	else pin_B2 = 0;

    	if((pin_B0 != pin_B0_temp || pin_B1 != pin_B1_temp || pin_B2 != pin_B2_temp) &&
			(pin_B0 == PIN_LOW || pin_B1 == PIN_LOW || pin_B2 == PIN_LOW)
    	){
    		if(pin_B2 == PIN_LOW && pin_B1 == PIN_LOW && pin_B0 == PIN_LOW)
    			button_pressed = ALL_BUTTON_PRESSED;
    		else if(pin_B2 == PIN_LOW && pin_B1 == PIN_LOW && pin_B0 == PIN_HIGH)
    			button_pressed = B1_AND_B2_PRESSED;
    		else if(pin_B2 == PIN_LOW && pin_B1 == PIN_HIGH && pin_B0 == PIN_LOW)
    			button_pressed = B0_AND_B2_PRESSED;
    		else if(pin_B2 == PIN_HIGH && pin_B1 == PIN_LOW && pin_B0 == PIN_LOW)
    			button_pressed = B0_AND_B1_PRESSED;
    		else if(pin_B2 == PIN_LOW && pin_B1 == PIN_HIGH && pin_B0 == PIN_HIGH)
    			button_pressed = B2_PRESSED;
    		else if(pin_B2 == PIN_HIGH && pin_B1 == PIN_LOW && pin_B0 == PIN_HIGH)
    			button_pressed = B1_PRESSED;
    		else if(pin_B2 == PIN_HIGH && pin_B1 == PIN_HIGH && pin_B0 == PIN_LOW)
    			button_pressed = B0_PRESSED;
    		else if(pin_B2 == PIN_HIGH && pin_B1 == PIN_HIGH && pin_B0 == PIN_HIGH)
    			button_pressed = NO_BUTTON_PRESSED;
    		else {}

#if 1
    		switch(button_pressed){
    		case ALL_BUTTON_PRESSED:
        		printf("All Button Pressed\r\n");
    			break;
    		case B1_AND_B2_PRESSED:
        		printf("B1 & B2 Pressed\r\n");
    			break;
    		case B0_AND_B2_PRESSED:
        		printf("B0 & B2 Pressed\r\n");
    			break;
    		case B0_AND_B1_PRESSED:
        		printf("B0 & B1 Pressed\r\n");
    			break;
    		case B2_PRESSED:
        		printf("B2 Pressed\r\n");
    			break;
    		case B1_PRESSED:
        		printf("B1 Pressed\r\n");
    			break;
    		case B0_PRESSED:
        		printf("B0 Pressed\r\n");
    			break;
    		case NO_BUTTON_PRESSED:
        		printf("No Button Pressed\r\n");
    			break;
    		}
#endif
//    		printf("B0 %d B1 %d B2 %d\r\n",pin_B0,pin_B1,pin_B2);
    	}

    	pin_B0_temp = pin_B0;
    	pin_B1_temp = pin_B1;
    	pin_B2_temp = pin_B2;

//    	HAL_Delay(300);
    }
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
	while(1){
		HAL_GPIO_TogglePin(GPIOE, GPIO_PIN_6);
		HAL_Delay(2000);
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
