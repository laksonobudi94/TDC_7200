/*
 * dci_ut_conf.h
 *
 *  Created on: Feb 10, 2021
 *      Author: laksono
 */

#ifndef INC_DCI_UT_CONF_H_
#define INC_DCI_UT_CONF_H_

/* 255 bit
*
* report log = 0 --> is no problem
*        					                       TIMEOUT 			  MAJORITY NOT REACHED
* IO_EXP_NOT_RESPONSE | TDC NOT RESPONSE | CH6 CH5 CH4 CH3 CH2 CH1 | CH6 CH5 CH4 CH3 CH2 CH1
*		13						12				    (11-6)					  (5-0)
*
* IO_EXP_NOT_RESPONSE
* Troubleshoot:
* - pastikan jalur i2c OK
*
* TDC NOT RESPONSE
* Troubleshoot:
* - pastikan jalur EN TDC & komunikasi SPI terhubung dengan baik
* - pastikan CLK source sesuai 8Mhz
* - pastikan power & mode sesuai
*
* MAJORITY NOT REACHED CHx
* Troubleshoot:
* - pastikan mounting ut probe bagus, tidak goyang, lurus, dan menekan dengan baik
*
* TIMEOUT CHx
* Troubleshoot:
* - pastikan koneksi kabel tx ut probe CHx ke tx sensoring terkoneksi dengan baik
* - pastikan koneksi kabel rx ut probe CHx ke rx sensoring terkoneksi dengan baik
* - pastikan koneksi pin START,STOP,INT TDC OK
*
*/

// reference:
// https://www.classltd.com/sound-velocity-table/
typedef enum
{/* material 		   m/s */
	VELO_INCONEL		= 5700,
	VELO_INCOLOY		= 7376,
	VELO_DUPLEX			= 5599,
	VELO_SUPER_DUPLEX	= 5599, /* ? */
	VELO_CARBON_STEEL	= 5931,
	VELO_SS316			= 5740,
	VELO_ALLUMUNIUM		= 6300,
	VELO_STEEL_MILD		= 5920
} velo_material;

typedef enum
{/* material*/
	MAT_INCONEL		= 0x01,
	MAT_INCOLOY,
	MAT_DUPLEX,
	MAT_SUPER_DUPLEX,
	MAT_CARBON_STEEL,
	MAT_SS316,
	MAT_ALLUMUNIUM,
	MAT_STEEL_MILD
} type_material;

typedef enum
{
	PROBE_5_MHZ		= 0x01,
	PROBE_10_MHZ
} type_probe;

typedef enum
{
	RESPONSE_OK			= 0x00,
	NOT_RECOGNIZE_CMD
} status_response;

typedef enum
{
	DATA_BALASAN_PREPARED= 0x00,
	NO_NEED_DATA_BALASAN
} status_balasan;

typedef enum
{
	PARSE_OK			= 0x00,
	CRC_NOT_SAME,
	PRE_HEADER_NOT_VALID,
} status_parse;

// HHU ===> UT
typedef enum
{
	DEVICE_CHECK_CMD	= 100,	/* cek status dci UT */
	SEND_CONF_CMD,				/* kirim config UT dari HHU (note: disertai struct config) */
	SEND_DATA_CMD,				/* minta UT untuk kirim data */
	READ_CONF_CMD,				/* baca config UT dari HHU */

	GOTO_CAL_MODE_CMD,			/* perintahkan UT untuk masuk ke mode calibration */
	EXIT_CAL_MODE_CMD,			/* perintahkan UT untuk masuk ke mode calibration */
	SAVE_CAL_VAL_CMD,			/* perintahkan UT untuk simpan nilai threshold */

	GOTO_THRESHOLD_COMP_CMD,
	EXIT_THRESHOLD_COMP_CMD,
	SAVE_THRESHOLD_COMP_CMD,
	SEND_THRESHOLD_COMP_VAL_CMD
} cmd_received;

/**
 * skenarion SET_THRESHOLD_COMP_CMD:
 *
 * Threshold dilakukan oleh HHU.
 * HHU akan mengirimkan nilai threshold comparator ke UT.
 * UT akan men-set nilai threshold, kemudian ambil data.
 * kirim kan kembali nilai threshold & thickness untuk memastikan.
 */

// UT ===> HHU
typedef enum
{
	DEVICE_STATUS_CMD	= 200,	/* kirim device status UT ke HHU */
	RESPONSE_CONF_CMD,			/* confirmation UT config (note: disertai struct config)*/
	TAKE_DATA_CMD,				/* kirim data pengukuran UT */
	RETRY_CMD,					/* minta HHU kirim cmd lagi */
	RESP_CAL_MODE_CMD,
	RESP_CAL_VAL_CMD,
	NOT_FOUND_CMD

} cmd_sending;

#define MAX_DATA_SEND 		100
#define OFFSET_DATA_BUFF	9
#define PRE_HEADER_SIZE 	5

struct setting_ut{
    uint8_t jum_ch; 		// 1-32
    uint8_t type_probe; 	// jenis tranducer 1=5Mhz / 2=10Mhz
	uint8_t interval_jam; 	// 0 - 255 jam
	uint8_t interval_menit; // 0 - 60 menit
	uint8_t jenis_material;
	uint16_t jml_data;
	unsigned long set_waktu;
    char tag_name[30];
};

struct cal_ut{
	float offset_comparator_sensoring;
	float offset_time_us_sensoring;
};

struct cmd_ut{
	uint8_t command;
	uint16_t byte_count;
	uint8_t status_code;
	uint32_t crc_value;
};

struct data_ut{
    uint8_t report_log; 		// indikasi jika ada masalah
    unsigned long time_stamp;	// waktu ambil data
    float hasil[6];
};

status_response response_cmd(struct cmd_ut *kom_dat_ut,uint8_t *resp_buff,uint16_t *byte_written);
status_parse parse_buff(uint8_t *buf,uint16_t size,struct cmd_ut *kom_dat_ut);
void dummy_data(uint8_t *buff_data_ut,uint16_t start_data,uint16_t jum);
status_balasan siapin_data_balasan(struct cmd_ut *kom_dat_ut,uint8_t *kom_buff);

#endif /* INC_DCI_UT_CONF_H_ */
