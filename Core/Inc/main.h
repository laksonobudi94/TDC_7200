/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define HHU_CONNECT_Pin GPIO_PIN_0
#define HHU_CONNECT_GPIO_Port GPIOC
#define CS_TDC_Pin GPIO_PIN_12
#define CS_TDC_GPIO_Port GPIOE
#define LCD_BL_Pin GPIO_PIN_10
#define LCD_BL_GPIO_Port GPIOB
#define LCD_PWR_Pin GPIO_PIN_11
#define LCD_PWR_GPIO_Port GPIOB
#define LCD_A0_Pin GPIO_PIN_12
#define LCD_A0_GPIO_Port GPIOB
#define RST_MCP_Pin GPIO_PIN_13
#define RST_MCP_GPIO_Port GPIOD
#define INT_TDC_Pin GPIO_PIN_5
#define INT_TDC_GPIO_Port GPIOD
#define STOP_TDC_Pin GPIO_PIN_6
#define STOP_TDC_GPIO_Port GPIOD
#define INT_PB_MCP_Pin GPIO_PIN_7
#define INT_PB_MCP_GPIO_Port GPIOD
#define INT_PB_MCP_EXTI_IRQn EXTI9_5_IRQn
/* USER CODE BEGIN Private defines */
uint8_t pin_B0,pin_B1,pin_B2;
uint8_t pin_B0_temp,pin_B1_temp,pin_B2_temp;
uint8_t portB;

typedef enum button_now
{
	NO_BUTTON_PRESSED = 0,
	B0_PRESSED,
	B1_PRESSED,
	B2_PRESSED,
	B0_AND_B1_PRESSED,
	B0_AND_B2_PRESSED,
	B1_AND_B2_PRESSED,
	ALL_BUTTON_PRESSED
} button_condition;

button_condition button_pressed;

#define PIN_LOW 0
#define PIN_HIGH 1

typedef enum gui_now
{
	MAIN_SCREEN = 0,
	SET_SCREEN,
	VIEW_CONF_SCREEN,
	CAL_SCREEN,
	FW_INFO_SCREEN,
	CALIBRATING_SCREEN,
	SAVE_CALIBRATED_SCREEN,
	OFFSET_COMP_SCREEN,
	SAVE_OFFSET_COMP_SCREEN
} gui_now_process;

gui_now_process state_gui;

#define CALIBRATION_OFFSET_VAL (float)0.064189189
#define MAJORITY_VALUE 50
#define NUM_CH_UT 6
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
