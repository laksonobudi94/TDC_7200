/*
 * design_gui.h
 *
 *  Created on: Feb 8, 2021
 *      Author: laksono
 */

#ifndef INC_DESIGN_GUI_H_
#define INC_DESIGN_GUI_H_

#include "u8g2.h"
#include "protocol_ut.h"
#include "epoch_time.h"

extern u8g2_t u8g2;

void tampil_init(void);
void set_menu(uint8_t num);
void view_calibrate(float *value);
void failed_cal(float value);
void calibrating(uint8_t ch,float value);
void view_fw_info();
void view_look_config(struct setting_ut view_conf,uint8_t page);
void configuration();
void view_ut_data(float *ut_ch,int data_ke);
void view_saved_calibrate(float *value);

#endif /* INC_DESIGN_GUI_H_ */
