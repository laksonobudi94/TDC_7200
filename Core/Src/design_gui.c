/*
 * design_gui.c
 *
 *  Created on: Feb 8, 2021
 *      Author: laksono
 */
#include "design_gui.h"



void tampil_init(void){
		u8g2_SetFont(&u8g2, u8g2_font_ncenB08_tr  );
		//sw=u8g2_GetStrWidth(&u8g2, buff);
}

void set_menu(uint8_t num){
	uint8_t buff[100];

	u8g2_ClearBuffer(&u8g2);

	sprintf(buff,"Menu");
	u8g2_DrawStr(&u8g2,50,9,buff);
	u8g2_DrawLine(&u8g2,0,11,128,11);
	sprintf(buff,"Back");
	u8g2_DrawStr(&u8g2,20,22,buff);
	sprintf(buff,"View Config");
	u8g2_DrawStr(&u8g2,20,32,buff);
	sprintf(buff,"Callibrate");
	u8g2_DrawStr(&u8g2,20,42,buff);
	sprintf(buff,"FW Info");
	u8g2_DrawStr(&u8g2,20,52,buff);
	sprintf(buff,"Configuration");
	u8g2_DrawStr(&u8g2,20,62,buff);

	uint8_t baris;

	sprintf(buff,"->");
	baris = ((num+1)*10)+12;
	u8g2_DrawStr(&u8g2,5,baris,buff);

	sprintf(buff,"  ");
	baris = ((num)*10)+12;
	u8g2_DrawStr(&u8g2,5,baris,buff);

	u8g2_UpdateDisplay(&u8g2);
}

void view_saved_calibrate(float *value){
	uint8_t buff[100];

	u8g2_ClearBuffer(&u8g2);

	sprintf(buff,"Calibrate");
	u8g2_DrawStr(&u8g2,39,9,buff);
	u8g2_DrawLine(&u8g2,0,11,128,11);
//	sprintf(buff,"Please Tap Tranducer");
//	u8g2_DrawStr(&u8g2,0,22,buff);
//	sprintf(buff,"CH1 into 5mm");
//	u8g2_DrawStr(&u8g2,0,32,buff);
	sprintf(buff,"            Calibrated");
	u8g2_DrawStr(&u8g2,0,42,buff);

	sprintf(buff,"Measure : %.2f mm",*value);
	u8g2_DrawStr(&u8g2,0,62,buff);

	u8g2_UpdateDisplay(&u8g2);
}
void view_calibrate(float *value){
	uint8_t buff[100];

	u8g2_ClearBuffer(&u8g2);

	sprintf(buff,"Calibrate");
	u8g2_DrawStr(&u8g2,39,9,buff);
	u8g2_DrawLine(&u8g2,0,11,128,11);
	sprintf(buff,"Please Tap Tranducer");
	u8g2_DrawStr(&u8g2,0,22,buff);
	sprintf(buff,"CH1 into 5mm");
	u8g2_DrawStr(&u8g2,0,32,buff);
	sprintf(buff,"Calibration Block");
	u8g2_DrawStr(&u8g2,0,42,buff);

	sprintf(buff,"Measure : %.2f mm",*value);
	u8g2_DrawStr(&u8g2,0,62,buff);

	u8g2_UpdateDisplay(&u8g2);
}

void view_threshold(float *measure,float *volt){
	uint8_t buff[100];

	u8g2_ClearBuffer(&u8g2);

	sprintf(buff,"Threshold Setting");
	u8g2_DrawStr(&u8g2,39,9,buff);
	u8g2_DrawLine(&u8g2,0,11,128,11);
	sprintf(buff,"Please Tap Tranducer");
	u8g2_DrawStr(&u8g2,0,22,buff);
	sprintf(buff,"CH1 into 5mm");
	u8g2_DrawStr(&u8g2,0,32,buff);
	sprintf(buff,"Calibration Block");
	u8g2_DrawStr(&u8g2,0,42,buff);

	sprintf(buff,"Threshold : %.2f volt",*volt);
	u8g2_DrawStr(&u8g2,0,62,buff);
	sprintf(buff,"Measure : %.2f mm",*measure);
	u8g2_DrawStr(&u8g2,0,62,buff);

	u8g2_UpdateDisplay(&u8g2);
}
void failed_cal(float value){
	uint8_t buff[100];

	u8g2_ClearBuffer(&u8g2);

	sprintf(buff,"Calibrate");
	u8g2_DrawStr(&u8g2,39,9,buff);
	u8g2_DrawLine(&u8g2,0,11,128,11);
	sprintf(buff,"Calibrating Failed!");
	u8g2_DrawStr(&u8g2,0,22,buff);
	sprintf(buff,"Try Again...");
	u8g2_DrawStr(&u8g2,0,32,buff);
	sprintf(buff,"Measure : %.2f mm",value);
	u8g2_DrawStr(&u8g2,0,62,buff);

	u8g2_UpdateDisplay(&u8g2);

}

void calibrating(uint8_t ch,float value){
	uint8_t buff[100];

	u8g2_ClearBuffer(&u8g2);

	sprintf(buff,"Calibrate");
	u8g2_DrawStr(&u8g2,39,9,buff);
	u8g2_DrawLine(&u8g2,0,11,128,11);
	sprintf(buff,"Calibrating...");
	u8g2_DrawStr(&u8g2,0,22,buff);
	sprintf(buff,"Tranducer %d",ch);
	u8g2_DrawStr(&u8g2,0,32,buff);
	sprintf(buff,"Measure : %.2f mm",value);
	u8g2_DrawStr(&u8g2,0,62,buff);

	u8g2_UpdateDisplay(&u8g2);

}

void view_fw_info(){
	uint8_t buff[100];

	u8g2_ClearBuffer(&u8g2);

	sprintf(buff,"FW info");
	u8g2_DrawStr(&u8g2,39,9,buff);
	u8g2_DrawLine(&u8g2,0,11,128,11);
	sprintf(buff,"Version: v2.0.0");
	u8g2_DrawStr(&u8g2,0,22,buff);
	sprintf(buff,"By Korosi Specindo");
	u8g2_DrawStr(&u8g2,0,42,buff);

	u8g2_UpdateDisplay(&u8g2);
}

/*
 * struct setting_ut{
    uint8_t jum_ch; v		// 1-32
    uint8_t type_probe; v 	// jenis tranducer 1=5Mhz / 2=10Mhz
	uint8_t interval_jam; v 	// 0 - 255 jam
	uint8_t interval_menit; v // 0 - 60 menit
	uint8_t jenis_material; v
	uint16_t jml_data; v
	unsigned long set_waktu; v
    char tag_name[30]; v
};
type_probe

 */
uint8_t byte_to_string_type_probe(uint8_t prb_dat,char *prb_str){
	uint8_t stats = 0;
	switch(prb_dat){
	case PROBE_5_MHZ:
		sprintf(prb_str,"5 MHZ PROBE");
		break;
	case PROBE_10_MHZ:
		sprintf(prb_str,"10 MHZ PROBE");
		break;
	default:
		stats = 1;
		break;
	}
	return stats;
}


uint8_t byte_to_string_material(uint8_t mat_dat,char *mat_str){
	uint8_t stats = 0;
	switch(mat_dat){
	case MAT_INCONEL:
		sprintf(mat_str,"INCONEL");
		break;
	case MAT_INCOLOY:
		sprintf(mat_str,"INCOLOY");
		break;
	case MAT_DUPLEX:
		sprintf(mat_str,"DUPLEX");
		break;
	case MAT_SUPER_DUPLEX:
		sprintf(mat_str,"SUPER_DUPLEX");
		break;
	case MAT_CARBON_STEEL:
		sprintf(mat_str,"CARBON_STEEL");
		break;
	case MAT_SS316:
		sprintf(mat_str,"SS316");
		break;
	case MAT_ALLUMUNIUM:
		sprintf(mat_str,"ALLUMUNIUM");
		break;
	case MAT_STEEL_MILD:
		sprintf(mat_str,"STEEL,MILD");
		break;
	default:
		stats = 1;
		break;
	}
	return stats;
}

uint8_t material_to_velo(uint8_t mat_dat,uint16_t *velo_mat){
	uint8_t stats = 0;
	switch(mat_dat){
	case MAT_INCONEL:
		*velo_mat = VELO_INCONEL;
		break;
	case MAT_INCOLOY:
		*velo_mat = VELO_INCOLOY;
		break;
	case MAT_DUPLEX:
		*velo_mat = VELO_DUPLEX;
		break;
	case MAT_SUPER_DUPLEX:
		*velo_mat = VELO_SUPER_DUPLEX;
		break;
	case MAT_CARBON_STEEL:
		*velo_mat = VELO_CARBON_STEEL;
		break;
	case MAT_SS316:
		*velo_mat = VELO_SS316;
		break;
	case MAT_ALLUMUNIUM:
		*velo_mat = VELO_ALLUMUNIUM;
		break;
	case MAT_STEEL_MILD:
		*velo_mat = VELO_STEEL_MILD;
		break;
	default:
		stats = 1;
		break;
	}
	printf("velo_mat = %d\r\n",*velo_mat);
	return stats;
}



void view_look_config(struct setting_ut view_conf,uint8_t page){
	uint8_t buff[100];
	uint8_t str_dat[30];

	u8g2_ClearBuffer(&u8g2);

	sprintf(buff,"View Config");
	u8g2_DrawStr(&u8g2,29,9,buff);
	u8g2_DrawLine(&u8g2,0,11,128,11);
	if(page == 0){
		sprintf(buff,"Tag: %s",view_conf.tag_name);
		u8g2_DrawStr(&u8g2,0,22,buff);
		sprintf(buff,"Interval: %d hr %d min",view_conf.interval_jam,view_conf.interval_menit);
		u8g2_DrawStr(&u8g2,0,32,buff);

		byte_to_string_material(view_conf.jenis_material,str_dat);
		sprintf(buff,"Material : %s",str_dat);
		u8g2_DrawStr(&u8g2,0,42,buff);
		uint16_t velocity_material;
		material_to_velo(view_conf.jenis_material,&velocity_material);
	//	printf("velocity_material = %d\r\n",velocity_material);
		sprintf(buff,"Velocity : %d m/s",velocity_material);
		u8g2_DrawStr(&u8g2,0,52,buff);

		sprintf(buff,">");
		u8g2_DrawStr(&u8g2,120,62,buff);
	}else if(page == 1){
		byte_to_string_type_probe(view_conf.type_probe,str_dat);
		sprintf(buff,"Type : %s",str_dat);
		u8g2_DrawStr(&u8g2,0,22,buff);
		sprintf(buff,"Num CH/Data: %d/%d",view_conf.jum_ch,view_conf.jml_data);
		u8g2_DrawStr(&u8g2,0,32,buff);
//		sprintf(buff,"Epoch: %d",view_conf.set_waktu);
//		u8g2_DrawStr(&u8g2,0,42,buff);

		struct DateTime set_date;
		convertUnixTimeToDate(view_conf.set_waktu, &set_date);
		sprintf(buff,"Date: %02d-%02d-%02d",set_date.day,set_date.month,set_date.year);
		u8g2_DrawStr(&u8g2,0,42,buff);

		sprintf(buff,"Time: %02d:%02d:%02d",set_date.hours,set_date.minutes,set_date.seconds);
		u8g2_DrawStr(&u8g2,0,52,buff);

		sprintf(buff,"<");
		u8g2_DrawStr(&u8g2,0,62,buff);

	}
	u8g2_UpdateDisplay(&u8g2);
}

void configuration(){
	uint8_t buff[100];

	u8g2_ClearBuffer(&u8g2);

	sprintf(buff,"View Config");
	u8g2_DrawStr(&u8g2,29,9,buff);
	u8g2_DrawLine(&u8g2,0,11,128,11);
	sprintf(buff,"Tag: UT1");
	u8g2_DrawStr(&u8g2,0,22,buff);
	sprintf(buff,"Interval: 1 hr 1 min");
	u8g2_DrawStr(&u8g2,0,32,buff);
	sprintf(buff,"Material : SS316");
	u8g2_DrawStr(&u8g2,0,42,buff);
	sprintf(buff,"Velocity : 5920 m/s");
	u8g2_DrawStr(&u8g2,0,62,buff);

	u8g2_UpdateDisplay(&u8g2);
}

void view_ut_data(float *ut_ch,int data_ke){
	uint8_t buff[100];

	//	  	cmd_lcd(0xAE);

	//	  	u8g2_ClearDisplay(&u8g2);
	u8g2_ClearBuffer(&u8g2);
	//sw=u8g2_GetStrWidth(&u8g2, buff);
	sprintf(buff,"Thickness(mm):");
	u8g2_DrawStr(&u8g2,5,9,buff);
	sprintf(buff,"Ch");
	u8g2_DrawStr(&u8g2,5,19,buff);
	sprintf(buff,"1:   %.2f",ut_ch[0]);
	u8g2_DrawStr(&u8g2,5,29,buff);
	sprintf(buff,"2:   %.2f",ut_ch[1]);
	u8g2_DrawStr(&u8g2,5,39,buff);
	sprintf(buff,"3:   %.2f",ut_ch[2]);
	u8g2_DrawStr(&u8g2,5,49,buff);
	sprintf(buff,"4:   %.2f",ut_ch[3]);
	u8g2_DrawStr(&u8g2,70,29,buff);
	sprintf(buff,"5:   %.2f",ut_ch[4]);
	u8g2_DrawStr(&u8g2,70,39,buff);
	sprintf(buff,"6:   %.2f",ut_ch[5]);
	u8g2_DrawStr(&u8g2,70,49,buff);

	sprintf(buff,"%d",data_ke);
	u8g2_DrawStr(&u8g2,5,60,buff);

	//	  	cmd_lcd(0xAF);
	u8g2_UpdateDisplay(&u8g2);
}

