/**
  ******************************************************************************
  * File Name          : OCTOSPI.c
  * Description        : This file provides code for the configuration
  *                      of the OCTOSPI instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "octospi.h"

/* USER CODE BEGIN 0 */
/**
 * W25Q128
 * 1 Block = 16 Sectors
 * 1 Sector = 16 Pages = 4096 Bytes
 * 1 Pages = 256 Bytes
*/
uint16_t W25QXX_TYPE = W25Q128;
uint8_t W25QXX_BUFFER[4096];

// we just need this 4 function
static uint8_t QSPI_WriteEnable(void);
static uint8_t QSPI_AutoPollingMemReady(void);
static uint8_t QSPI_Configuration(void);
static uint8_t QSPI_ResetChip(void);

#ifndef HAL_OPSI_TIMEOUT_DEFAULT_VALUE
#define HAL_OPSI_TIMEOUT_DEFAULT_VALUE ((uint32_t)30000)/* 5 s */
#endif

/* USER CODE END 0 */

OSPI_HandleTypeDef hospi1;

/* OCTOSPI1 init function */
void MX_OCTOSPI1_Init(void)
{
  OSPIM_CfgTypeDef OSPIM_Cfg_Struct = {0};

  hospi1.Instance = OCTOSPI1;
  hospi1.Init.FifoThreshold = 4;
  hospi1.Init.DualQuad = HAL_OSPI_DUALQUAD_DISABLE;
  hospi1.Init.MemoryType = HAL_OSPI_MEMTYPE_MICRON;
  hospi1.Init.DeviceSize = 23;
  hospi1.Init.ChipSelectHighTime = 2;
  hospi1.Init.FreeRunningClock = HAL_OSPI_FREERUNCLK_DISABLE;
  hospi1.Init.ClockMode = HAL_OSPI_CLOCK_MODE_0;
  hospi1.Init.WrapSize = HAL_OSPI_WRAP_NOT_SUPPORTED;
  hospi1.Init.ClockPrescaler = 2;
  hospi1.Init.SampleShifting = HAL_OSPI_SAMPLE_SHIFTING_NONE;
  hospi1.Init.DelayHoldQuarterCycle = HAL_OSPI_DHQC_DISABLE;
  hospi1.Init.ChipSelectBoundary = 0;
  if (HAL_OSPI_Init(&hospi1) != HAL_OK)
  {
    Error_Handler();
  }
  OSPIM_Cfg_Struct.ClkPort = 1;
  OSPIM_Cfg_Struct.NCSPort = 1;
  OSPIM_Cfg_Struct.IOLowPort = HAL_OSPIM_IOPORT_1_LOW;
  if (HAL_OSPIM_Config(&hospi1, &OSPIM_Cfg_Struct, HAL_OSPI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
  {
    Error_Handler();
  }

}

#if 0
void HAL_OSPI_MspInit(OSPI_HandleTypeDef* ospiHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(ospiHandle->Instance==OCTOSPI1)
  {
  /* USER CODE BEGIN OCTOSPI1_MspInit 0 */

  /* USER CODE END OCTOSPI1_MspInit 0 */
    /* OCTOSPI1 clock enable */
    __HAL_RCC_OSPIM_CLK_ENABLE();
    __HAL_RCC_OSPI1_CLK_ENABLE();
  
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**OCTOSPI1 GPIO Configuration    
    PA2     ------> OCTOSPIM_P1_NCS
    PA3     ------> OCTOSPIM_P1_CLK
    PA6     ------> OCTOSPIM_P1_IO3
    PA7     ------> OCTOSPIM_P1_IO2
    PB0     ------> OCTOSPIM_P1_IO1
    PB1     ------> OCTOSPIM_P1_IO0 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_6|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF10_OCTOSPIM_P1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF10_OCTOSPIM_P1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF10_OCTOSPIM_P1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* USER CODE BEGIN OCTOSPI1_MspInit 1 */

  /* USER CODE END OCTOSPI1_MspInit 1 */
  }
}

void HAL_OSPI_MspDeInit(OSPI_HandleTypeDef* ospiHandle)
{

  if(ospiHandle->Instance==OCTOSPI1)
  {
  /* USER CODE BEGIN OCTOSPI1_MspDeInit 0 */

  /* USER CODE END OCTOSPI1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_OSPIM_CLK_DISABLE();
    __HAL_RCC_OSPI1_CLK_DISABLE();
  
    /**OCTOSPI1 GPIO Configuration    
    PA2     ------> OCTOSPIM_P1_NCS
    PA3     ------> OCTOSPIM_P1_CLK
    PA6     ------> OCTOSPIM_P1_IO3
    PA7     ------> OCTOSPIM_P1_IO2
    PB0     ------> OCTOSPIM_P1_IO1
    PB1     ------> OCTOSPIM_P1_IO0 
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_6|GPIO_PIN_7);

    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_0|GPIO_PIN_1);

  /* USER CODE BEGIN OCTOSPI1_MspDeInit 1 */

  /* USER CODE END OCTOSPI1_MspDeInit 1 */
  }
} 
#endif

/* USER CODE BEGIN 1 */

/* QUADSPI init function */
uint8_t CSP_QUADSPI_Init(void) {
	//prepare QSPI peripheral for ST-Link Utility operations
	if (HAL_OSPI_DeInit(&hospi1) != HAL_OK) {
		return 1;
	}

	MX_OCTOSPI1_Init();

	if (QSPI_ResetChip() != HAL_OK) {
		return 2;
	}

	HAL_Delay(1);

	/* 1- Read W25QXX Flash ID */
	if(W25QXX_ReadID() != W25Q128)
		return 3;

	if (QSPI_Configuration() != HAL_OK) {
		return 4;
	}

	return HAL_OK;
}

uint8_t QSPI_Configuration(void) {
	uint8_t status;
	uint8_t test_buffer[3] = { 0 };

	/*read status register 1-3*/
    status = QSPI_Send_CMD(W25X_ReadStatusReg1, 0x00, 0, HAL_OSPI_ADDRESS_NONE, HAL_OSPI_DATA_1_LINE, 3);
    if(status != HAL_OK)
    		return HAL_ERROR;
	if (HAL_OSPI_Receive(&hospi1, test_buffer,
			HAL_OPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
		return HAL_ERROR;
	}

	/*write status register 2*/
    W25QXX_Write_Enable();

	/*write status register 2*/
    status = QSPI_Send_CMD(W25X_WriteStatusReg2, 0x00, 0, HAL_OSPI_ADDRESS_NONE, HAL_OSPI_DATA_1_LINE, 1);
	if(status != HAL_OK)
		return HAL_ERROR;

	// S9 --> Quad Enable
	// S15-S8
	test_buffer[0] = 2;
	if (HAL_OSPI_Transmit(&hospi1, test_buffer,
			HAL_OPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
		return HAL_ERROR;
	}

	// harus di cek lagi
    status = QSPI_Send_CMD(W25X_ReadStatusReg2, 0x00, 0, HAL_OSPI_ADDRESS_NONE, HAL_OSPI_DATA_1_LINE, 1);
    if(status != HAL_OK)
    		return HAL_ERROR;
	if (HAL_OSPI_Receive(&hospi1, test_buffer,
			HAL_OPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
		return HAL_ERROR;
	}

	return HAL_OK;
}

uint8_t QSPI_WriteEnable(void) {
	/* Enable write operations ------------------------------------------ */
	W25QXX_Write_Enable();
	W25QXX_Write_Enabled();
	return HAL_OK;
}

uint8_t QSPI_AutoPollingMemReady(void) {

	W25QXX_Wait_Busy();

	return HAL_OK;
}

uint8_t CSP_QSPI_EraseSector(uint32_t EraseStartAddress, uint32_t EraseEndAddress) {
	W25QXX_Erase_Sector(EraseStartAddress);

	return HAL_OK;
}

uint8_t CSP_OSPI_Erase_Chip(void){
#if 1
	uint8_t status;
	W25QXX_Write_Enable();

	if(W25QXX_Write_Enabled() !=HAL_OK)
		return HAL_ERROR;

	status = QSPI_Send_CMD(W25X_ChipErase, 0, 0, HAL_OSPI_ADDRESS_NONE, HAL_OSPI_DATA_NONE, 1);
	if(status != HAL_OK)
		return HAL_ERROR;

	if(W25QXX_Wait_Busy() !=HAL_OK)
		return HAL_ERROR;
#else
	int var;

	#ifndef SECTORS_COUNT
	#define SECTORS_COUNT 100
	#endif

	for (var = 0; var < MEMORY_SECTOR_SIZE-1; var++) {

	      if (CSP_QSPI_EraseSector(var * MEMORY_SECTOR_SIZE,
	                               (var + 1) * MEMORY_SECTOR_SIZE - 1) != HAL_OK) {

	          while (1)
	        	  ;  //breakpoint - error detected
	      }
	  }

#endif

	return HAL_OK;
}

uint8_t CSP_QSPI_WriteMemory(uint8_t* buffer, uint32_t address,uint32_t buffer_size) {
	W25QXX_Write(buffer, address, buffer_size);
	return HAL_OK;
}

uint8_t CSP_QSPI_EnableMemoryMappedMode(void) {

	OSPI_RegularCmdTypeDef sCommand;
	OSPI_MemoryMappedTypeDef sMemMappedCfg;

	/* Enable Memory-Mapped mode-------------------------------------------------- */
	sCommand.InstructionMode = HAL_OSPI_INSTRUCTION_1_LINE;
	sCommand.AddressSize = HAL_OSPI_ADDRESS_24_BITS;
	sCommand.AlternateBytesMode = HAL_OSPI_ALTERNATE_BYTES_NONE;
	sCommand.SIOOMode = HAL_OSPI_SIOO_INST_EVERY_CMD;
	sCommand.Instruction = W25X_QuadPageProgram;
	sCommand.AddressMode = HAL_OSPI_ADDRESS_1_LINE;
	sCommand.Address = 0;
	sCommand.DataMode = HAL_OSPI_DATA_4_LINES;
	sCommand.DummyCycles = 0;
	sCommand.DQSMode = HAL_OSPI_DQS_ENABLE;
	sCommand.DataDtrMode = HAL_OSPI_DATA_DTR_DISABLE;
	sCommand.NbData = 1;
	sCommand.AlternateBytesSize = HAL_OSPI_ALTERNATE_BYTES_8_BITS;
	sCommand.AlternateBytesDtrMode = HAL_OSPI_ALTERNATE_BYTES_DTR_DISABLE;
	sCommand.AlternateBytes = 0;
	sCommand.AddressDtrMode = HAL_OSPI_ADDRESS_DTR_DISABLE;
	sCommand.InstructionDtrMode = HAL_OSPI_INSTRUCTION_DTR_DISABLE;
	sCommand.InstructionSize = HAL_OSPI_INSTRUCTION_8_BITS;
	sCommand.OperationType = HAL_OSPI_OPTYPE_WRITE_CFG;
	sCommand.FlashId = HAL_OSPI_FLASH_ID_1;

	if (QSPI_WriteEnable() != HAL_OK) {
		return 1;
	}

	if (HAL_OSPI_Command(&hospi1, &sCommand, HAL_OPSI_TIMEOUT_DEFAULT_VALUE)
			!= HAL_OK) {
		return 2;
	}

	/* Enable Memory-Mapped mode-------------------------------------------------- */
	sCommand.Instruction = W25X_QoutFastRead;
	sCommand.DummyCycles = 8;
	sCommand.DQSMode = HAL_OSPI_DQS_DISABLE;
	sCommand.OperationType = HAL_OSPI_OPTYPE_READ_CFG;

	if (HAL_OSPI_Command(&hospi1, &sCommand, HAL_OPSI_TIMEOUT_DEFAULT_VALUE)
			!= HAL_OK) {
		return 3;
	}

	sMemMappedCfg.TimeOutActivation = HAL_OSPI_TIMEOUT_COUNTER_ENABLE;
	sMemMappedCfg.TimeOutPeriod = 0x20;
	if (HAL_OSPI_MemoryMapped(&hospi1, &sMemMappedCfg) != HAL_OK) {
		return 4;
	}

	return HAL_OK;
}

uint8_t QSPI_ResetChip() {
	uint8_t status;

	/* Erasing Sequence -------------------------------------------------- */
	status = QSPI_Send_CMD(W25X_ResetEnable, 0x00, 0, HAL_OSPI_ADDRESS_NONE, HAL_OSPI_DATA_NONE, 0);
	if(status != HAL_OK)
		return HAL_ERROR;

	uint32_t temp = 0;

	for (temp = 0; temp < 0x2f; temp++) {
		__NOP();
	}

	status = QSPI_Send_CMD(W25X_ResetExecute, 0x00, 0, HAL_OSPI_ADDRESS_NONE, HAL_OSPI_DATA_NONE, 0);
	if(status != HAL_OK)
		return HAL_ERROR;

	return HAL_OK;
}

///// Rapikan Code

/**
 * @brief	QSPI�?��?命令
 *
 * @param   instruction		�?�?��?的指令
 * @param   address			�?��?到的目的地�?�
 * @param   dummyCycles		空指令周期数
 * @param   addressMode		地�?�模�?; QSPI_ADDRESS_NONE,QSPI_ADDRESS_1_LINE,QSPI_ADDRESS_2_LINES,QSPI_ADDRESS_4_LINES
 * @param   dataMode		数�?�模�?; QSPI_DATA_NONE,QSPI_DATA_1_LINE,QSPI_DATA_2_LINES,QSPI_DATA_4_LINES
 * @param   dataSize        待传输的数�?�长度
 *
 * @return  uint8_t			QSPI_OK:正常
 *                      QSPI_ERROR:错误
 */
uint8_t QSPI_Send_CMD(uint32_t instruction, uint32_t address,uint32_t dummyCycles,
                    uint32_t addressMode, uint32_t dataMode, uint32_t dataSize)
{
	OSPI_RegularCmdTypeDef sCommand;

    sCommand.InstructionMode = HAL_OSPI_INSTRUCTION_1_LINE;
	sCommand.AddressSize = HAL_OSPI_ADDRESS_24_BITS;
	sCommand.AlternateBytesMode = HAL_OSPI_ALTERNATE_BYTES_NONE;
	sCommand.SIOOMode = HAL_OSPI_SIOO_INST_EVERY_CMD;
	sCommand.Instruction = instruction;
	sCommand.AddressMode = addressMode;
	sCommand.Address = address;
	sCommand.DataMode = dataMode;
	sCommand.DummyCycles = dummyCycles;
	sCommand.DQSMode = HAL_OSPI_DQS_DISABLE;
	sCommand.DataDtrMode = HAL_OSPI_DATA_DTR_DISABLE;
	sCommand.NbData = dataSize;
	sCommand.AlternateBytesSize = HAL_OSPI_ALTERNATE_BYTES_8_BITS;
	sCommand.AlternateBytesDtrMode = HAL_OSPI_ALTERNATE_BYTES_DTR_DISABLE;
	sCommand.AlternateBytes = 0;
	sCommand.AddressDtrMode = HAL_OSPI_ADDRESS_DTR_DISABLE;
	sCommand.InstructionDtrMode = HAL_OSPI_INSTRUCTION_DTR_DISABLE;
	sCommand.InstructionSize = HAL_OSPI_INSTRUCTION_8_BITS;
	sCommand.OperationType = HAL_OSPI_OPTYPE_COMMON_CFG;
	sCommand.FlashId = HAL_OSPI_FLASH_ID_1;

    if(HAL_OSPI_Command(&hospi1, &sCommand, HAL_OPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
      return QSPI_ERROR;

    return QSPI_OK;
}

/**
 * @brief	读�?�W25QXX芯片ID
 *
 * @param   void
 *
 * @return  uint16_t    返回值如下
 * 				0XEF13,表示芯片型�?�为W25Q80
 * 				0XEF14,表示芯片型�?�为W25Q16
 * 				0XEF15,表示芯片型�?�为W25Q32
 * 				0XEF16,表示芯片型�?�为W25Q64
 * 				0XEF17,表示芯片型�?�为W25Q128
 * 				0XEF18,表示芯片型�?�为W25Q256
 */
uint16_t W25QXX_ReadID(void)
{
    uint8_t ID[2];
    uint16_t deviceID;

    QSPI_Send_CMD(W25X_ManufactDeviceID, 0x00, 0, HAL_OSPI_ADDRESS_1_LINE, HAL_OSPI_DATA_1_LINE, sizeof(ID));
    HAL_OSPI_Receive(&hospi1, ID, HAL_OPSI_TIMEOUT_DEFAULT_VALUE);

    deviceID = (ID[0] << 8) | ID[1];

    return deviceID;
}

/**
 * @brief	读�?�SPI FLASH数�?�
 *
 * @param   pBuf			数�?�存储区
 * @param   ReadAddr		开始读�?�的地�?�(最大32bit)
 * @param   ReadSize	    �?读�?�的字节数(最大32bit)
 *
 * @return  void
 */
uint8_t W25QXX_Read(uint8_t *pBuf, uint32_t ReadAddr, uint32_t ReadSize)
{
	//QSPI_Send_CMD(W25X_ReadData, ReadAddr, 0, QSPI_ADDRESS_1_LINE, QSPI_DATA_1_LINE, ReadSize);
    QSPI_Send_CMD(W25X_FastReadQuad, ReadAddr, 8, HAL_OSPI_ADDRESS_1_LINE, HAL_OSPI_DATA_4_LINES, ReadSize);

    if(HAL_OSPI_Receive(&hospi1, pBuf, HAL_OPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
        return W25QXX_ERROR;

    return W25QXX_OK;
}

/**
 * @brief	等待空闲
 *
 * @param   void
 *
 * @return  void
 */
uint8_t W25QXX_Wait_Busy(void)
{
#if 1
	uint8_t status = 1;

    while((status & 0x01) == 0x01){
        if(QSPI_Send_CMD(W25X_ReadStatusReg1, 0x00, 0, HAL_OSPI_ADDRESS_NONE, HAL_OSPI_DATA_1_LINE, 1) != HAL_OK)
        	return HAL_ERROR;
        if(HAL_OSPI_Receive(&hospi1, &status, HAL_OPSI_TIMEOUT_DEFAULT_VALUE)!=HAL_OK)
        	return HAL_ERROR;
    }
#else
    QSPI_Send_CMD(W25X_ReadStatusReg1, 0x00, 0, HAL_OSPI_ADDRESS_NONE, HAL_OSPI_DATA_1_LINE, 1);

	/* Configure automatic polling mode to wait for memory ready ------ */
	OSPI_AutoPollingTypeDef sConfig;
	sConfig.Match = 0x00;
	sConfig.Mask = 0x01;
    sConfig.MatchMode     = HAL_OSPI_MATCH_MODE_AND;
    sConfig.AutomaticStop = HAL_OSPI_AUTOMATIC_STOP_ENABLE;
    sConfig.Interval = 0;

	if (HAL_OSPI_AutoPolling(&hospi1,&sConfig,
	HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
		return HAL_ERROR;
	}
#endif

	return HAL_OK;

}

uint8_t W25QXX_Write_Enabled(void)
{
#if 1
    uint8_t status = 1;

    while((status & 0x02) == 0x02){
        if(QSPI_Send_CMD(W25X_ReadStatusReg1, 0x00, 0, HAL_OSPI_ADDRESS_NONE, HAL_OSPI_DATA_1_LINE, 1) != HAL_OK)
        	return HAL_ERROR;
        if(HAL_OSPI_Receive(&hospi1, &status, HAL_OPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
         	return HAL_ERROR;
    }
#else
    QSPI_Send_CMD(W25X_ReadStatusReg1, 0x00, 0, HAL_OSPI_ADDRESS_NONE, HAL_OSPI_DATA_1_LINE, 1);

	/* Configure automatic polling mode to wait for memory ready ------ */
	OSPI_AutoPollingTypeDef sConfig;
	sConfig.Match = 0x02;
	sConfig.Mask = 0x02;
	if (HAL_OSPI_AutoPolling(&hospi1,&sConfig,
	HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK) {
		return HAL_ERROR;
	}
#endif
	return HAL_OK;
}

/**
 * @brief	擦除一个扇区
 *
 * @param   EraseAddr �?擦除的扇区地�?�
 *
 * @return  void
 */
void W25QXX_Erase_Sector(uint32_t EraseAddr)
{
    W25QXX_Write_Enable();
    W25QXX_Wait_Busy();

    QSPI_Send_CMD(W25X_SectorErase, EraseAddr, 0, HAL_OSPI_ADDRESS_1_LINE, HAL_OSPI_DATA_NONE, 1);
    W25QXX_Wait_Busy();
}

/**
 * @brief	在指定地�?�开始写入最大一页的数�?�
 *
 * @param   pBuf			数�?�存储区
 * @param   WriteAddr		开始写入的地�?�(最大32bit)
 * @param   WriteSize	    �?写入的字节数(最大1 Page = 256 Bytes),该数�?应该超过该页的剩余字节数
 *
 * @return  void
 */
void W25QXX_Write_Page(uint8_t *pBuf, uint32_t WriteAddr, uint32_t WriteSize)
{
    if(WriteSize > W25X_PAGE_SIZE)
        return;

    W25QXX_Write_Enable();
    W25QXX_Wait_Busy();

    //QSPI_Send_CMD(W25X_PageProgram, WriteAddr, 0, QSPI_ADDRESS_1_LINE, QSPI_DATA_1_LINE, WriteSize);
    QSPI_Send_CMD(W25X_QuadPageProgram, WriteAddr, 0, HAL_OSPI_ADDRESS_1_LINE, HAL_OSPI_DATA_4_LINES, WriteSize);
    HAL_OSPI_Transmit(&hospi1, pBuf, HAL_OPSI_TIMEOUT_DEFAULT_VALUE);
    W25QXX_Wait_Busy();
}

/**
 * @brief	无检验写SPI FLASH
 * 			必须确�?所写的地�?�范围内的数�?�全部为0XFF,�?�则在�?�0XFF处写入的数�?�将失败!
 * 			具有自动�?�页功能
 * 			在指定地�?�开始写入指定长度的数�?�,但是�?确�?地�?��?越界!
 *
 * @param   pBuf			数�?�存储区
 * @param   WriteAddr		开始写入的地�?�(最大32bit)
 * @param   WriteSize	    �?写入的字节数(最大32bit)
 *
 * @return  void
 */
void W25QXX_Write_NoCheck(uint8_t *pBuf, uint32_t WriteAddr, uint32_t WriteSize)
{
    uint32_t pageremain = W25X_PAGE_SIZE - WriteAddr % W25X_PAGE_SIZE; //�?�页剩余的字节数

    if(WriteSize <= pageremain)
        pageremain = WriteSize;

    while(1)
    {
        W25QXX_Write_Page(pBuf, WriteAddr, pageremain);

        if(WriteSize == pageremain)
            break;              //写入结�?�了
        else                    //WriteSize > pageremain
        {
            pBuf += pageremain;
            WriteAddr += pageremain;
            WriteSize -= pageremain;

            pageremain = (WriteSize > W25X_PAGE_SIZE) ? W25X_PAGE_SIZE : WriteSize;
        }
    }
}

/**
 * @brief	写SPI FLASH
 * 			在指定地�?�开始写入指定长度的数�?�
 * 			该函数带擦除�?作!
 *
 * @param   pBuf			数�?�存储区
 * @param   WriteAddr		开始写入的地�?�(最大32bit)
 * @param   WriteSize	    �?写入的字节数(最大32bit)
 *
 * @return  void
 */
void W25QXX_Write(uint8_t *pBuf, uint32_t WriteAddr, uint32_t WriteSize)
{
    uint32_t secpos = WriteAddr / W25X_SECTOR_SIZE; //扇区地�?�
    uint32_t secoff = WriteAddr % W25X_SECTOR_SIZE; //在扇区内的�??移
    uint32_t secremain = W25X_SECTOR_SIZE - secoff; //扇区剩余空间大�?
    uint32_t i;
    uint8_t * W25QXX_BUF = W25QXX_BUFFER;

    if(WriteSize <= secremain)
        secremain = WriteSize; //�?大于4096个字节

    while(1)
    {
        W25QXX_Read(W25QXX_BUF, secpos * W25X_SECTOR_SIZE, W25X_SECTOR_SIZE); //读出整个扇区的内容

        for(i = 0; i < secremain; i++) //校验数�?�
        {
            if(W25QXX_BUF[secoff + i] != 0XFF)
                break; //需�?擦除
        }

        if(i < secremain) //需�?擦除
			{
				W25QXX_Erase_Sector(secpos * W25X_SECTOR_SIZE);//擦除这个扇区

				for(i = 0; i < secremain; i++)	 //�?制
				{
					W25QXX_BUF[i + secoff] = pBuf[i];
				}

				W25QXX_Write_NoCheck(W25QXX_BUF, secpos * W25X_SECTOR_SIZE, W25X_SECTOR_SIZE); //写入整个扇区

			}
			else
				W25QXX_Write_NoCheck(pBuf, WriteAddr, secremain); //写已�?擦除了的,直接写入扇区剩余区间.

        if(WriteSize == secremain)
            break; //写入结�?�了
        else//写入未结�?�
        {
            secpos++;       //扇区地�?�增1
            secoff = 0;     //�??移�?置为0

            pBuf += secremain;          //指针�??移
            WriteAddr += secremain;     //写地�?��??移
            WriteSize -= secremain;		//字节数递�?

            secremain = (WriteSize > W25X_SECTOR_SIZE) ? W25X_SECTOR_SIZE : WriteSize;
        }
    }
}

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
